<?php


return [
    'url' => env('STRAPI_URL'),


    'token' => env('STRAPI_TOKEN', null),];
