<?php $__env->startSection('title',); ?>

<?php $__env->startSection('content'); ?>



    <div class="container container-fluid pt-5 my-5">
        <h1 class="text-center display-4 fw-bold text-center red pt-5"><?php echo e($service['title']); ?></h1>
        <p class="px-5 pt-5 text-center"><?php echo e($service['subtitle']); ?></p>
        <div class="my-4 pb-5 text-center" data-aos="fade-up" data-aos-delay="150">
            <a href="<?php echo e(route('seo.demande-devis', ['service' => $service['title']])); ?>" class="btn btn-primary falling-button" data-button-id="2">Demande de devis</a>
        </div>
    </div>

    <div class="container-fluid pt-5 px-5 pb-5 bg-white">
        <div class="container row">
            <div class="col-md-6">
                <div class="ms-5 me-5">
                    <h2 class="py-5 red fw-bold"><?php echo e($service['title_paragraph']); ?></h2>
                    <?php echo $htmlContent; ?>

                </div>
            </div>
            <div class="col-md-6 align-self-center">
                <div class="text-center pb-5">
                    <?php if(isset($service['media']) && isset($service['media']['data'][0])): ?>
                            <?php $image = $service['media']['data'][0]['attributes']; ?>
                        <img src="<?php echo e($strapiImageUrl . $image['formats']['small']['url']); ?>" alt="<?php echo e($service['title']); ?>" class="img-fluid pt-5 max-width-100">
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>



    <div class="container pb-5">
        <div class="container-fluid mt-5 pb-5  ">
            <h1 class="py-5 fw-bold text-left m-2 "><?php echo e($service['title_section_avantage']); ?></h1>
            <?php $__currentLoopData = $service['avantages']['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $avantage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="container-fluid mt-5 ">
                    <h1 class="red-icon mb-3 "><?php echo $avantage['attributes']['icon']; ?></h1>
                    <h4 class="red "><?php echo e($avantage['attributes']['title']); ?></h4>
                    <p  ><?php echo e($avantage['attributes']['content']); ?></p>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>


    <div class=" container-fluid pt-5 px-5 pb-5 bg-white">
        <div class="container bg-white">
            <div class="row">
                <h1 class="fw-bold text-center mb-5"><?php echo e($service['title_section_etape']); ?></h1>
                <div class="col-md-6">
                    <?php $__currentLoopData = $service['etapes']['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $etape): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="container-fluid mt-5">
                            <h6>
                                <i class="bi bi-caret-right-fill red-icon"></i>
                                <span class="text-dark"><?php echo e($etape['attributes']['title']); ?></span>
                            </h6>
                            <p><?php echo e($etape['attributes']['content']); ?></p>
                        </div>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <div class="col-md-6 d-flex align-items-center justify-content-center">
                    <?php if(isset($service['media']['data'][1])): ?>
                            <?php $image = $service['media']['data'][1]['attributes']; ?>
                        <img src="<?php echo e($strapiImageUrl .$image['formats']['large']['url']); ?>" alt="<?php echo e($service['title']); ?>" class="img-fluid pt-5 my-auto max-width-100">
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <style>
        .extra-horizontal-margin {
            margin-left: 90px;
            margin-right: 90px;
        }
    </style>

    <div class="container-fluid mt-5 pt-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10">
                <h2 class="text-center fw-bold mb-4 red display-6 pb-5">Ils nous ont fait confiance</h2> <!-- Titre ajouté -->
                <div class="row extra-horizontal-margin mt-5">
                    <div class="col-md-4">
                        <img src="<?php echo e(asset("img/entreprises/bonhommie.png")); ?>" class="d-block img-fluid mx-auto mb-5" alt="Image de Bonhommie">
                    </div>
                    <div class="col-md-4">
                        <img src="<?php echo e(asset("img/entreprises/eb.png")); ?>" class="d-block img-fluid mx-auto mb-5" alt="Image d'EB">
                    </div>
                    <div class="col-md-4">
                        <img src="<?php echo e(asset("img/entreprises/parisnanterre.png")); ?>" class="d-block img-fluid mx-auto mb-5" alt="Image de Paris Nanterre">
                    </div>
                </div>
                <div class="row extra-horizontal-margin">
                    <div class="col-md-4">
                        <img src="<?php echo e(asset("img/entreprises/mcs.png")); ?>" class="d-block img-fluid mx-auto mb-5" alt="Image de reaut">
                    </div>
                    <div class="col-md-4">
                        <img src="<?php echo e(asset("img/entreprises/t4ex-1.png")); ?>" class="d-block img-fluid mx-auto mb-5" alt="Image t4EX">
                    </div>
                    <div class="col-md-4">
                        <img src="<?php echo e(asset("img/entreprises/ref3systemin.png")); ?>" class="d-block img-fluid mx-auto mt-3" style="max-width: 50%" alt="Image NKAMA GYUDON">
                    </div>
                </div>
            </div>
        </div>
        <div class="my-4 pb-5 text-center" data-aos="fade-up" data-aos-delay="150">
            <a href="<?php echo e(route('seo.demande-devis', ['service' => $service['title']])); ?>" class="btn btn-primary falling-button" data-button-id="2">Demande de devis</a>
        </div>
    </div>
    <div class="container-fluid  text-center mt-5 mb-5 p-5">
        <div class="accordion accordion-nofill" id="accordionFlushExample">
            <?php $__currentLoopData = $service['q_and_as']['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $q_and_a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="accordion-item ms-md-5 ms-lg-0">
                    <h2 class="accordion-header card-transparent">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse<?php echo e($key + 1); ?>" aria-expanded="false" aria-controls="flush-collapse<?php echo e($key + 1); ?>" style="font-weight: bold; font-size: 26px">
                            <?php echo e($q_and_a['attributes']['title']); ?>

                        </button>
                    </h2>

                    <div id="flush-collapse<?php echo e($key + 1); ?>" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body" style="font-size: 16px; text-align: left"><?php echo e($q_and_a['attributes']['content']); ?></div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>



<?php $__env->stopSection(); ?>




<?php echo $__env->make('layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/lily-rose/PhpstormProjects/site-web/resources/views/seo/services/show.blade.php ENDPATH**/ ?>