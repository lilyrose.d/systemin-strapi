<?php $__env->startSection('content'); ?>

    <div class="container pt-5 mb-5 pb-5">
        <div class="container-fluid pt-5 container-background my-5">
            <div class="text-center">
                <h1 class="display-4 fw-bold">Solutions Métiers</h1>
                <p class="shorter-border mt-4"></p>
                <p class="mt-5 me-5">Découvrez comment Systemin transforme divers secteurs grâce à ses technologies innovantes.</p>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="row row-cols-1 row-cols-md-2 g-4"> <!-- Utilisation de classes pour une disposition en colonnes sur les petits écrans -->
                        <div class="col mb-4">
                            <div class="thumbnail p-3">
                                <img src="<?php echo e(asset("img/entreprises/bonhommie.png")); ?>" class="img-fluid mb-3" alt="Image 1">
                                <h2 class="fw-bold grey">Application de Gestion de Fichiers de Tarifs pour Propriétés Viticoles</h2>
                                <p class="article-text mb-3">Notre application innovante, spécialement conçue pour Bonhommie, une agence de distribution de vins et spiritueux basée à Blanquefort dans le bordelais, améliore la gestion des tarifs de propriétés viticoles.</p>
                                <a href="<?php echo e(route("seo.secteurs-activites.bonhommie")); ?>" class="btn btn-third">Découvrir le projet</a>
                            </div>
                        </div>
                        <div class="col mb-4">
                            <div class="thumbnail p-3">
                                <img src="<?php echo e(asset("img/entreprises/irragori.png")); ?>" class="img-fluid mb-3" alt="Image 2">
                                <h2 class="fw-bold grey">Application de Gestion de Biens Immobiliers</h2>
                                <p class="article-text mb-3">Notre application, conçue pour l’agence immobilière Irragori, est une solution pratique de gestion de biens immobiliers, incluant des appartements, des maisons et des terrains.</p>
                                <a href="<?php echo e(route("seo.secteurs-activites.irragori")); ?>" class="btn btn-third">Découvrir le projet</a>
                            </div>
                        </div>
                        <div class="col mb-4">
                            <div class="thumbnail p-3">
                                <img src="<?php echo e(asset("img/entreprises/irep.png")); ?>" class="img-fluid mb-3" alt="Image 1">
                                <h2 class="fw-bold grey">Application Web de Gestion de Remboursements de Réparation</h2>
                                <p class="article-text mb-3">Notre projet consiste à développer une Application Web pour notre client iRep, afin de simplifier et d’automatiser la gestion des remboursements liés aux réparations. Cette application permettra d’intégrer facilement la gestion des réparations et des remboursements dans plusieurs API, améliorant ainsi l’efficacité des processus.</p>
                                <a href="<?php echo e(route("seo.secteurs-activites.irep")); ?>" class="btn btn-third">Découvrir le projet</a>
                            </div>
                        </div>
                        <div class="col mb-4">
                            <div class="thumbnail p-3">
                                <img src="<?php echo e(asset("img/entreprises/reaut.png")); ?>" class="img-fluid mb-3" alt="Image 1">
                                <h2 class="fw-bold grey">API Entre Logiciel Métier et Framework E-commerce</h2>
                                <p class="article-text mb-3">Notre projet vise à développer une API pour Château Réaut, qui permettra de relier efficacement leur logiciel métier à la plateforme e-commerce. Cette API éliminera la saisie répétitive de commandes en interne, simplifiant ainsi la gestion des commandes et améliorant la productivité de l’entreprise en automatisant le processus de bout en bout.</p>
                                <a href="#" class="btn text-color">Projet en cours</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/lily-rose/PhpstormProjects/site-web/resources/views/seo/secteur-activite.blade.php ENDPATH**/ ?>