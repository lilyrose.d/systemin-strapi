<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Contact de <?php echo e($firstname); ?> </title>
    <style>
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
        }
        .container {
            margin: 0 auto;
            padding: 20px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Nouvelle demande de devis <?php echo e($service); ?></h1>


        <div>
            <p><strong>Email :</strong> <?php echo e($email); ?></p>
            <p><strong>Téléphone :</strong> <?php echo e($phone); ?></p>
        </div>


        <div>
            <h2>Message :</h2>
            <?php echo e($messages); ?>

        </div>

        <p>Cordialement,<br>
        <?php echo e($firstname); ?> </p>
    </div>
</body>
</html>
<?php /**PATH /Users/lily-rose/PhpstormProjects/site-web/resources/views/emails/devis.blade.php ENDPATH**/ ?>