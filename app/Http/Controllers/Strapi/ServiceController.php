<?php

namespace App\Http\Controllers\Strapi;

use Illuminate\Support\Facades\Http;
use League\CommonMark\CommonMarkConverter;

class ServiceController
{
    public function service($id)
    {
        $strapiUrl = env('STRAPI_URL');
        $strapiImageUrl = env('STRAPI_IMAGE_URL');

        $response = Http::get("{$strapiUrl}/services/{$id}?populate=*");


        if ($response->successful()) {
            $service = $response->json();

            if (!empty($service['data'])) {
                $serviceData = $service['data']['attributes'];
                $htmlContent = isset($serviceData['paragraph']) ? (new CommonMarkConverter())->convertToHtml($serviceData['paragraph']) : null;

                return view('seo.services.show', [
                    'service' => $serviceData,
                    'slug' => $serviceData['slug'],
                    'strapiImageUrl' => $strapiImageUrl,
                    'htmlContent' => $htmlContent,
                ]);
            } else {
                return redirect()->route('erreur');
            }
        } else {
            return redirect()->route('erreur');
        }
    }

    public function serviceBySlug($slug)
    {
        $strapiUrl = env('STRAPI_URL');
        $strapiImageUrl = env('STRAPI_IMAGE_URL');

        $response = Http::get("{$strapiUrl}/services?populate=*");

        if ($response->successful()) {
            $services = $response->json()['data'];


            foreach ($services as $service) {
                if ($service['attributes']['slug'] === $slug) {
                    $serviceData = $service['attributes'];
                    $htmlContent = isset($serviceData['paragraph']) ? (new CommonMarkConverter())->convertToHtml($serviceData['paragraph']) : null;

                    return view('seo.services.show', [
                        'service' => $serviceData,
                        'slug' => $slug,
                        'strapiImageUrl' => $strapiImageUrl,
                        'htmlContent' => $htmlContent,
                    ]);
                }
            }
        }

        return redirect()->route('erreur');
    }

   }
