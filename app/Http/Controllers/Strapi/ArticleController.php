<?php

namespace App\Http\Controllers\Strapi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use League\CommonMark\CommonMarkConverter;
use Illuminate\Support\Str;

class ArticleController
{

    public function index(Request $request)
    {
        $strapiUrl = env('STRAPI_URL');
        $response = Http::get("{$strapiUrl}/articles/?populate=*&_sort=createdAt:DESC");


        if ($response->successful()) {
            $articles = collect($response->json()['data']);

            // Trier les articles par date de création décroissante
            $articles = $articles->sortByDesc(function ($article) {
                return $article['attributes']['createdAt'];
            });

            // Convertir la collection triée en tableau
            $articles = $articles->values()->all();

            // Récupérer toutes les catégories
            $categoriesResponse = Http::get("{$strapiUrl}/categories");
            $categories = $categoriesResponse->json()['data'];

            $selectedCategory = $request->input('category');


            if (!empty($articles)) {

                if (!empty($selectedCategory)) {
                    $filteredArticles = [];
                    foreach ($articles as $article) {

                        if (isset($article['attributes']['categorie']['data']['id']) && $article['attributes']['categorie']['data']['id'] == $selectedCategory) {
                            $filteredArticles[] = $article;
                        }
                    }
                    $articles = $filteredArticles;
                }



                foreach ($articles as &$article) {
                    $article['attributes']['slug'] = Str::slug($article['attributes']['title']);
                    // Convertir le contenu Markdown en HTML
                    $htmlContent = isset($article['attributes']['content']) ? (new CommonMarkConverter())->convertToHtml($article['attributes']['content']) : null;

                    // Récupérer les paragraphes de l'article
                    $paragraphs = [];
                    if (isset($article['attributes']['paragraphs']['data'])) {
                        foreach ($article['attributes']['paragraphs']['data'] as $paragraph) {
                            $paragraphData = $paragraph['attributes'];

                            $paragraphs[] = [
                                'content' => $paragraphData['content'],
                            ];
                        }
                    }


                    $article['attributes']['htmlContent'] = $htmlContent;
                    $article['attributes']['paragraphs'] = $paragraphs;
                }

                return view('seo.articles.index', [
                    'articles' => $articles,
                    'categories' => $categories,
                    'selectedCategory' => $request->input('category'),
                ]);
            } else {

                return redirect()->route('erreur');
            }
        } else {

            return redirect()->route('erreur');
        }
    }



    public function showBySlug($slug)
    {
        $strapiUrl = env('STRAPI_URL');
        $strapiImageUrl = env('STRAPI_IMAGE_URL');

        $response = Http::get("{$strapiUrl}/articles/?populate=*");

        if ($response->successful()) {
            $articles = $response->json()['data'];

            if (!empty($articles)) {
                foreach ($articles as $article) {
                    $articleSlug = Str::slug($article['attributes']['title']);
                    if ($articleSlug === $slug) {
                        $articleData = $article['attributes'];
                        $htmlContent = isset($articleData['content']) ? (new CommonMarkConverter())->convertToHtml($articleData['content']) : null;

                        // Récupérer les paragraphes de l'article avec les médias associés
                        $paragraphs = [];
                        if (isset($articleData['paragraphs']['data'])) {
                            foreach ($articleData['paragraphs']['data'] as $paragraph) {
                                $paragraphId = $paragraph['id'];
                                $paragraphResponse = Http::get("{$strapiUrl}/paragraphs/{$paragraphId}?populate=media");
                                if ($paragraphResponse->successful()) {
                                    $paragraphData = $paragraphResponse->json()['data']['attributes'];

                                    $htmlContent = isset($paragraphData['content']) ? (new CommonMarkConverter())->convertToHtml($paragraphData['content']) : null;


                                    // Récupérer les médias associés à ce paragraphe
                                    $media = null;
                                    if (isset($paragraphData['media']['data'][0]['attributes'])) {
                                        $media = $paragraphData['media']['data'][0]['attributes'];
                                    }

                                    $paragraphs[] = [
                                        'title' =>  $paragraphData['title'],
                                        'htmlContent' => $htmlContent,
                                        'media' => $media,
                                    ];

                                }
                            }


                        }

                        return view('seo.articles.show', [
                            'article' => $articleData,
                            'slug' => $slug,
                            'strapiImageUrl' => $strapiImageUrl,
                            'htmlContent' => $htmlContent,
                            'paragraphs' => $paragraphs,
                        ]);
                    }
                }
            }
        }

        return null;
    }


    public function welcome()
    {
        $strapiUrl = env('STRAPI_URL');
        $response = Http::get("{$strapiUrl}/articles/?populate=*");

        if ($response->successful()) {
            $articles = $response->json()['data'];

            if (!empty($articles)) {
                foreach ($articles as &$article) {
                    $article['attributes']['slug'] = Str::slug($article['attributes']['title']);
                    // Convertir le contenu Markdown en HTML
                    $htmlContent = isset($article['attributes']['content']) ? (new CommonMarkConverter())->convertToHtml($article['attributes']['content']) : null;

                    // Récupérer les paragraphes de l'article
                    $paragraphs = [];
                    if (isset($article['attributes']['paragraphs']['data'])) {
                        foreach ($article['attributes']['paragraphs']['data'] as $paragraph) {
                            $paragraphData = $paragraph['attributes'];

                            $paragraphs[] = [
                                'content' => $paragraphData['content'],
                            ];
                        }
                    }


                    $article['attributes']['htmlContent'] = $htmlContent;
                    $article['attributes']['paragraphs'] = $paragraphs;
                    $article['attributes']['url'] = route('seo.content.show', ['slug' => $article['attributes']['slug']]);
                }

            return view('welcome', [
                'articles' => $articles,
                'paragraphs' => $paragraphs,
            ]);
            } else {

                return redirect()->route('erreur');
            }
        } else {

            return redirect()->route('erreur');
        }
    }
}

