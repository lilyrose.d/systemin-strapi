<?php

namespace App\Http\Controllers\Strapi;


use App\Http\Controllers\Controller;

class ContentController extends Controller
{
    public function showBySlug($slug)
    {

        $articleController = new ArticleController();
        $article = $articleController->showBySlug($slug);

        if ($article) {
            return $article;
        }


        $serviceController = new ServiceController();
        $service = $serviceController->serviceBySlug($slug);

        if ($service) {
            return $service;
        }


        return redirect()->route('erreur');
    }
}
