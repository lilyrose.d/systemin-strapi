<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;

class EmailController extends Controller
{
    public function send(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'phone' => ['required', 'regex:/^[0-9]+$/'],
            'service' => 'nullable|string',
            'message' => 'required|string',
            'file' => 'nullable|file|mimes:pdf',
        ]);


        $details = [
            'firstname' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'service' => $request->input('service'),
            'message' => $request->input('message'),
        ];

        try {
            $email = env('MAIL_RECEIVER', 'lilyrose.duffau@gmail.com');


            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $fileName = $file->store('uploads', 'public');
                $details['file'] = $fileName;
            }

            $mail = new ContactMail($details);
            Mail::to($email)->send($mail);

            return back()->with('success', 'Votre message a été envoyé avec succès!');
        } catch (\Exception $e) {
            return back()->withErrors($e->getMessage());
        }
    }
}
