<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Contact Mail',
        );
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->view('emails.devis')
            ->with([
                'firstname' => $this->details['firstname'],
                'email' => $this->details['email'],
                'service' => $this->details['service'],
                'messages' => $this->details['message'],
                'phone' => $this->details['phone'],
            ]);


        if (isset($this->details['file'])) {
            $mail->attach(storage_path('app/public/' . $this->details['file']));
        }

        return $mail;
    }
}
