function checkFadeIns() {


    fadeIns.forEach(fadeIn => {
        const rect = fadeIn.getBoundingClientRect();
        const windowHeight = window.innerHeight;



        if (rect.top < windowHeight * 0.9) {

            fadeIn.classList.add('active');
        }
    });
}

window.addEventListener('scroll', function() {

    checkFadeIns();
});

window.addEventListener('resize', function() {
    console.log("Resize event detected"); // Vérifie si l'événement de redimensionnement de la fenêtre est détecté
    checkFadeIns();
});

checkFadeIns(); // Vérifie si la fonction est appelée au chargement de la page
