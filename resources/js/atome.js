// Fonction pour générer un nombre aléatoire dans une plage donnée
function randomInRange(min, max) {
    return Math.random() * (max - min) + min;
}

// Fonction pour dessiner un trait entre deux points
function drawLine(ctx, x1, y1, x2, y2) {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
}

// Fonction pour dessiner les points et les relier avec des traits
function drawConnections() {
    const canvas = document.getElementById('canvas');
    const ctx = canvas.getContext('2d');

    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;

    const numPoints = 50; // Nombre de points à générer

    // Générer et dessiner les points
    const points = [];
    for (let i = 0; i < numPoints; i++) {
        const x = randomInRange(0, canvas.width);
        const y = randomInRange(0, canvas.height);
        points.push({ x, y });
        ctx.fillRect(x, y, 2, 2); // Dessiner le point
    }

    // Relier les points avec des traits
    for (let i = 0; i < points.length; i++) {
        for (let j = i + 1; j < points.length; j++) {
            const distance = Math.sqrt(Math.pow(points[i].x - points[j].x, 2) + Math.pow(points[i].y - points[j].y, 2));
            if (distance < 100) { // Longueur maximale du trait
                ctx.strokeStyle = '#ffcc00'; // Couleur du trait
                ctx.lineWidth = 0.5;
                drawLine(ctx, points[i].x, points[i].y, points[j].x, points[j].y); // Dessiner le trait
            }
        }
    }
}

// Appeler la fonction pour dessiner les points et les relier avec des traits lorsque la page est chargée
window.onload = function() {
    drawConnections();
};
