document.addEventListener('DOMContentLoaded', function() {
    var fallingButtons = document.querySelectorAll('.falling-button');

    function checkVisibility() {
        fallingButtons.forEach(function(button) {
            var rect = button.getBoundingClientRect();
            var windowHeight = window.innerHeight || document.documentElement.clientHeight;
            var buttonId = button.dataset.buttonId;


            if (rect.top < windowHeight && rect.bottom >= 0) {
                button.classList.add('falling-animation-' + buttonId);
            }
        });
    }

    checkVisibility();
    window.addEventListener('scroll', checkVisibility);
});
