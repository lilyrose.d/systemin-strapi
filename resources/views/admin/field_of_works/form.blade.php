@extends('admin.admin')


@section('title', $field_of_work->exists ? "Editer un domaine de travail": "Créer un domaine de travail")

@section('content')

    <h1>@yield('title')</h1>

    <form action="{{route($field_of_work->exists ? 'admin.field_of_work.update' : 'admin.field_of_work.store', $field_of_work) }}"method="post">

        @csrf
        @method($field_of_work->exists ? 'put':"post")


        @include ('shared.input',['name'=> 'name','label'=>'Nom','value'=>$field_of_work->name])


        <div>
            <button class="btn btn-primary">
                @if($field_of_work->exists)
                    Modifier
                @else
                    Créer
                @endif
            </button>

        </div>
    </form>
@endsection
