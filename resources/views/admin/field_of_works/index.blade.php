@extends('admin.admin')

@section('title','Tous les domaines de travail ')

@section('content')

    <div class="d-flex justify-content-between align-items-center">
    <h1> @yield('title')</h1>
        <a href="{{route('admin.field_of_work.create')}}" class="btn btn-primary">Ajouter un domaine de travail </a>
    </div>

    <table class="table table-strped">
        <thead>
        <tr>
            <th>Nom</th>
            <th class="text-end">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($field_of_works as $field_of_work)
            <tr>
                <td>{{$field_of_work->name}}</td>
                <td>
                    <div class="d-flex gap-2 w-100 justify-content-end">
                        <a href="{{route('admin.field_of_work.edit', $field_of_work)}}" class="btn btn-primary">Editer</a>
                        <form action="{{route('admin.field_of_work.destroy', $field_of_work)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Supprimer</button>
                        </form>
                    </div>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{$field_of_works->links()}}
@endsection

