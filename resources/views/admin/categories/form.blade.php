@extends('admin.admin')


@section('title', $category->exists ? "Editer une catégorie": "Créer une catégorie")

@section('content')

    <h1>@yield('title')</h1>

    <form action="{{route($category->exists ? 'admin.category.update' : 'admin.category.store', $category) }}"method="post">

        @csrf
        @method($category->exists ? 'put':"post")


        @include ('shared.input',['name'=> 'name','label'=>'Nom','value'=>$category->name])

        <div>
            <button class="btn btn-primary">
                @if($category->exists)
                    Modifier
                @else
                    Créer
                @endif
            </button>

        </div>
    </form>
@endsection
