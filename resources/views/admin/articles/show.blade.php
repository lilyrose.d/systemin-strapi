
@extends('admin.admin')

@section('title', 'Détails de l\'article')

@section('content')
    <h1>{{ $article->title }}</h1>
    <h2>Catégorie: {{ $article->category->name }}</h2>

    <h2>Paragraphe:</h2>
    <div class="content">
        @foreach($article->paragraphs as $paragraph)
            <p>{{ $paragraph->paragraph_content }}</p>
        @endforeach
    </div>

    <a href="{{ route('admin.article.edit', $article) }}" class="btn btn-primary">Éditer l'article</a>
    <form action="{{ route('admin.article.destroy', $article) }}" method="post">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger">Supprimer l'article</button>
    </form>
@endsection
