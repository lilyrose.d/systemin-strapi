@extends('admin.admin')

@section('title', $article->exists ? "Éditer un article" : "Créer un article")

@section('content')
    <h1>@yield('title')</h1>

    <form action="{{ $article->exists ? route('admin.article.update', $article) : route('admin.article.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        @if($article->exists)
            @method('PUT')
        @endif

        <div class="mb-3">
            <label for="title" class="form-label">Titre</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ old('title', $article->title) }}">
        </div>

        <div class="mb-3">
            <label for="category_id" class="form-label">Catégorie</label>
            <select class="form-control" id="category_id" name="category_id">
                <option value="">Sélectionnez une catégorie</option>
                @foreach($categories as $id => $name)
                    <option value="{{ $id }}" {{ $id == $article->category_id ? 'selected' : '' }}>{{ $name }}</option>
                @endforeach
            </select>
        </div>


        <div class="mb-3">
            <label for="image" class="form-label">Image</label>
            <input type="file" class="form-control" id="image" name="image">
        </div>

        @if($article->image)
            <div class="mb-3">
                <label>Image actuelle :</label><br>
                <img src="{{asset('/storage/images/' .$article->image) }}" alt="Image actuelle" class="img-fluid" style="max-width: 50%; height: 200px">

            </div>
        @endif
        <button type="submit" class="btn btn-primary">{{ $article->exists ? 'Modifier' : 'Créer' }}</button>

    </form>


    @if($article->exists)
        <div class="mb-3">
            @foreach ($paragraphs as $paragraph)
                <div class="paragraph">
                    @include('admin.paragraphs.form_template')
                    <form action="{{ route('admin.paragraph.destroy', $paragraph->id) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Supprimer</button>

                    </form>
                </div>
            @endforeach
        </div>



    <div class="mb-3">
            <a href="{{ route('admin.paragraph.create', ['article' => $article->id]) }}" class="btn btn-primary">Ajouter un paragraphe</a>
        </div>
    @endif
@endsection
