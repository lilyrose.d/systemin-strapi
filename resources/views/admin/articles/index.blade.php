@extends('admin.admin')

@section('title', 'Tous les articles')

@section('content')
    <div class="d-flex justify-content-between align-items-center">
        <h1>@yield('title')</h1>
        <a href="{{ route('admin.article.create') }}" class="btn btn-primary">Ajouter un article</a>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Titre</th>
            <th>Catégorie</th>
            <th class="text-end">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($articles as $article)
            <tr>
                <td>{{ $article->title }}</td>
                <td>{{ $article->category->name }}</td>
                <td class="text-end">
                    <div class="btn-group" role="group">
                        <a href="{{ route('admin.article.edit', $article) }}" class="btn btn-primary"> Éditer </a>
                        <form action="{{ route('admin.article.destroy', $article) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger"> Supprimer </button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $articles->links() }}
@endsection
