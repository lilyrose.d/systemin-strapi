

    <form action="{{ isset($paragraph) ? route('admin.paragraph.update', $paragraph->id) : route('admin.paragraph.store') }}" method="post">
        @csrf
        <input type="hidden" class="form-label" name="articleId" id="articleId" value="{{ $article->id }}">
        <div class="mb-3">
            <label for="title" class="form-label">Titre</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ isset($paragraph) ? $paragraph->title : old('title') }}">
        </div>
        <div class="mb-3">
            <label for="paragraph_content" class="form-label">Contenu</label>
            <textarea class="form-control reset-height" id="paragraph_content" name="paragraph_content">{!! isset($paragraph) ? $paragraph->paragraph_content : old('paragraph_content') !!}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">{{ isset($paragraph) ? 'Modifier' : 'Ajouter' }} un paragraphe</button>
    </form>
