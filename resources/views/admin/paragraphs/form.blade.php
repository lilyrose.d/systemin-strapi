@extends('admin.admin')

@section('content')
    <h1>{{ isset($paragraph) ? 'Modifier' : 'Ajouter' }} un paragraphe</h1>
    @include('admin.paragraphs.form_template')
    <a href="{{ route('admin.article.edit', ['article' => $article->id]) }}" class="btn btn-secondary">Retour</a>
@endsection
