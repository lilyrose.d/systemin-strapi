@extends('admin.admin')


@section('title', $job_type->exists ? "Editer un type d'emploi": "Créer un type d'emploi")

@section('content')

    <h1>@yield('title')</h1>

    <form action="{{route($job_type->exists ? 'admin.job_type.update' : 'admin.job_type.store', $job_type) }}"method="post">

        @csrf
        @method($job_type->exists ? 'put':"post")


        @include ('shared.input',['name'=> 'name','label'=>'Nom','value'=>$job_type->name])


        <div>
            <button class="btn btn-primary">
                @if($job_type->exists)
                    Modifier
                @else
                    Créer
                @endif
            </button>

        </div>
    </form>
@endsection
