@extends('admin.admin')

@section('title','Tous les types d\'emplois ')

@section('content')

    <div class="d-flex justify-content-between align-items-center">
    <h1> @yield('title')</h1>
        <a href="{{route('admin.job_type.create')}}" class="btn btn-primary">Ajouter un type d'emploi </a>
    </div>

    <table class="table table-strped">
        <thead>
        <tr>
            <th>Nom</th>
            <th class="text-end">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($job_types as $job_type)
            <tr>
                <td>{{$job_type->name}}</td>
                <td>
                    <div class="d-flex gap-2 w-100 justify-content-end">
                        <a href="{{route('admin.job_type.edit', $job_type)}}" class="btn btn-primary">Editer</a>
                        <form action="{{route('admin.job_type.destroy', $job_type)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Supprimer</button>
                        </form>
                    </div>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{$job_types->links()}}
@endsection

