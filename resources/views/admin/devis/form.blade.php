@extends('admin.admin')


@section('title', $devi->exists ? "Editer un devis": "Créer un devis")

@section('content')

    <h1>@yield('title')</h1>

    <form action="{{route($devi->exists ? 'admin.devis.update' : 'admin.devis.store', $devi) }}" method="post">

        @csrf
        @method($devi->exists ? 'put':"post")


        @include ('shared.input',['name'=> 'surname','label'=>'Nom','value'=>$devi->surname])
        @include ('shared.input',['name'=> 'mail','label'=>'Mail','value'=>$devi->mail])
        @include ('shared.input',['name'=> 'phone_number','label'=>'Numéro de téléphone','value'=>$devi->phone_number])

        @include ('shared.selectService',['name'=> 'services','label'=>'Services','value'=>$devi->services()->pluck('id'), 'services'=> $services])

        @include ('shared.input',['name'=> 'content','label'=>'Message','value'=>$devi->content])


        <div>
            <button class="btn btn-primary">
                @if($devi->exists)
                    Modifier
                @else
                    Créer
                @endif
            </button>

        </div>
    </form>
@endsection
