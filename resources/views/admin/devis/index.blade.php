@extends('admin.admin')

@section('title','Tous les devis')

@section('content')

    <div class="d-flex justify-content-between align-items-center">
    <h1> @yield('title')</h1>
        <a href="{{route('admin.devis.create')}}" class="btn btn-primary">Ajouter un Devis</a>
    </div>

    <table class="table table-strped">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Mail</th>
            <th>Numéro de téléphone</th>
            <th>Demande de service</th>
            <th class="text-end">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($devis as $devi)
            <tr>
                <td>{{$devi->surname}}</td>
                <td>{{$devi->mail}}</td>
                <td>{{$devi->phone_number}}</td>
                <td>
                    @foreach($devi->services as $service)
                        {{ $service->title }}
                    @endforeach
                </td>

                <td>
                    <div class="d-flex gap-2 w-100 justify-content-end">
                        <a href="{{route('admin.devis.edit', $devi)}}" class="btn btn-primary">Editer</a>
                        <form action="{{route('admin.devis.destroy', $devi)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Supprimer</button>
                        </form>
                    </div>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{$devis->links()}}
@endsection

