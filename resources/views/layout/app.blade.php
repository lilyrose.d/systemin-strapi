<!DOCTYPE html>
<html lang="fr" xmlns="http://www.w3.org/1999/html">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css">



    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>@yield('title')</title>


    <link rel="icon" type="image/x-icon" href="{{asset("img/logo/Union-1.png")}}" />
</head>

<body>

    <div class="container container-fluid ">
            <div class="bg-white px-5 fixed-top ">
                <nav class="navbar navbar-expand-lg navbar-light container-fluid">
                    <a class="navbar-brand ml-0" href="{{ route('welcome') }}"><img src="{{ asset('img/logo/logo_systemin.png') }}" alt="Logo" style="max-width: 150px;"></a>
                    <button class="navbar-toggler clicked" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" id="navbarToggleBtn">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-start" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown"  id="servicesDropdown">
                                <a class="nav-link dropdown-toggle  dropdown-hover" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Services
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    @foreach($services['data'] as $service)
                                        <li><a class="dropdown-item" href="{{ route('seo.content.show', ['slug' => $service['attributes']['slug']]) }}">{{ $service['attributes']['title'] }}</a></li>
                                    @endforeach

                                    <li><a class="dropdown-item" href="{{ route('seo.services.maintenance-et-infogerance') }}">Maintenance et infogérance</a></li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('seo.secteur-activite')}}">Secteurs d'activité</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('seo.articles.index')}}">Articles</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('seo.a-propos')}}">À propos</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                    <div>
                        <!-- Session Status -->
                        @if(session()->has('success'))
                            <div class="alert alert-success">

                                {{ session()->get('success') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                    </div>
            </div>
    </div>

        <div>
            @yield('content')

        </div>

<footer class="container container-fluid py-4 mt-5 pt-5 ">
    <div class="row">
        <div class="col-md-3 ">
            <img src="{{asset("img/logo/logo_systemin.png")}}" alt="Logo" style="max-width: 150px;" class="d-inline-block align-text-top ms-3">
            <p class="mt-4 ms-3 me-3" style=" font-size: 14px">Nous sommes spécialisés dans le développement d'applications informatiques proposant des solutions personnalisées, alliant conception, programmation et intégration.</p>
        </div>
        <div class="col-md-3">
            <h4 class="h5">Plan du site</h4>
            <ul class="list-unstyled">
                <li><a href="{{route('welcome')}}">Accueil</a></li>
                <li><a href="{{route('seo.articles.index')}}">Articles Techniques</a></li>
                <li><a href="{{route('seo.a-propos')}}">A Propos</a></li>
                <li><a href="{{route('seo.recrutement')}}">Recrutement</a></li>
                <li><a href="{{route('seo.secteur-activite')}}">Secteurs d'activité</a></li>
                <li><a href="{{route('seo.demande-devis')}}">Devis</a></li>
                <li><a href="{{route('seo.cgv')}}">CGV</a></li>
                <li><a href="{{route('seo.mentions-legales')}}">Mentions Légales et Politique de Confidentialité </a></li>
            </ul>
        </div>
        <div class="col-md-3">
            <h4 class="h5">Services</h4>
            <ul class="list-unstyled">
                @foreach($services['data'] as $service)
                    <li><a href="{{ route('seo.content.show', ['slug' => $service['attributes']['slug']]) }}">{{ $service['attributes']['title'] }}</a></li>
                @endforeach
                <li><a href="{{ route('seo.services.maintenance-et-infogerance') }}">Maintenance et infogérance</a></li>
            </ul>
        </div>
        <div class="col-md-3 pb-5">
            <h2 class="h5">A PROPOS</h2>
            <p>Systemin Bordeaux, France</p>
        </div>

    </div>

</footer>
<div class="container-fluid background-footer mt-5 pb-1">
    <p class="text-center text-white pt-2 " >&copy;Systemin</p>
    <p class=" text-center text-white" >Tous droits réservés.</p>
</div>



</body>

</html>
