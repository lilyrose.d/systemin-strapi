@extends('layout.app')

@section('content')

    <div class="container pt-5 mt-5 pb-5">
        <div class="container-fluid">
            <div class="text-center">
                <h2 class="red fw-bold">Mentions légales et politique de confidentialité</h2>
            </div>
        </div>

        <div class="container-fluid mt-5 pt-5">
            <h5 class="pb-4">Mentions légales</h5>
            <p><strong>Nom de l’entreprise :</strong> Systemin</p>
            <p><strong>Forme juridique :</strong> Micro-entreprise</p>
            <p><strong>Numéro SIRET :</strong> 799201926</p>
            <p><strong>Adresse du siège social :</strong> 8 RUE EDOUARD LALO 33520 BRUGES</p>
            <p>Coordonnées de contact :</p>
            <ul>
                <li>E-mail : nico[at]systemin.fr</li>
                <li>Directeur de la publication : Nicolas Cestari</li>
            </ul>
            <p>Hébergeur du site web : O2switch</p>
            <p>Adresse : Chem. des Pardiaux, 63000 Clermont-Ferrand</p>
            <p>Téléphone : 04 44 44 60 40</p>
            <p>E-mail : support@o2switch.fr</p>
            <h5>Conditions générales de vente ou d’utilisation :</h5>
            <p><a href="https://systemin.fr/conditions-generales-de-ventes/">https://systemin.fr/conditions-generales-de-ventes/</a></p>
            <h3>Politique de confidentialité</h3>
            <p><strong>Date de dernière mise à jour :</strong> 15/04/2023</p>
            </div>
            <h5>1. Collecte des informations personnelles</h5>
            <p>Nous collectons des informations personnelles lorsque vous nous les fournissez volontairement, notamment lorsque vous nous contactez par e-mail, téléphone, ou à travers notre site web. Les informations personnelles que nous collectons peuvent inclure, sans s’y limiter, les éléments suivants :</p>
            <ul>
                <li>Nom</li>
                <li>Adresse e-mail</li>
                <li>Numéro de téléphone</li>
                <li>Informations de contact professionnelles</li>
                <li>Autres informations que vous choisissez de nous fournir</li>
            </ul>
            <h5>2. Utilisation des informations personnelles</h5>
            <p>Nous utilisons les informations personnelles que vous nous fournissez dans le but de répondre à vos demandes, de vous fournir nos services, de gérer notre relation commerciale avec vous, et de nous conformer à nos obligations légales.</p>
            <h5>3. Partage des informations personnelles</h5>
            <p>Nous ne partageons pas vos informations personnelles avec des tiers, sauf dans les cas suivants :</p>
            <ul>
                <li>Lorsque cela est nécessaire pour répondre à votre demande ou pour fournir nos services.</li>
                <li>Lorsque cela est exigé par la loi ou par une autorité gouvernementale.</li>
                <li>Lorsque cela est nécessaire pour protéger nos droits, notre vie privée, notre sécurité ou notre propriété, ainsi que ceux de nos clients ou du public.</li>
                <li>Lorsque cela est nécessaire pour enquêter, prévenir ou prendre des mesures concernant des activités illégales, frauduleuses ou potentiellement préjudiciables.</li>
            </ul>
            <h5>4. Sécurité des informations personnelles</h5>
            <p>Nous mettons en place des mesures de sécurité raisonnables pour protéger vos informations personnelles contre la perte, l’accès non autorisé, la divulgation, l’altération ou la destruction. Cependant, aucune méthode de transmission ou de stockage électronique n’est totalement sécurisée, et nous ne pouvons garantir la sécurité absolue de vos informations.</p>
            <h5>5. Vos droits</h5>
            <p>Vous avez le droit d’accéder à vos informations personnelles, de les corriger, de les supprimer ou de vous opposer à leur utilisation. Pour exercer ces droits, veuillez nous contacter à l’adresse dpo@systemin.fr.</p>
            <h5>6. Modifications de la politique de confidentialité</h5>
            <p>Nous pouvons mettre à jour cette politique de confidentialité de temps en temps pour refléter les changements dans nos pratiques en matière de protection des données. La date de la dernière mise à jour sera indiquée en haut de cette page.</p>
            <h5>7. Nous contacter</h5>
            <p>Si vous avez des questions concernant notre politique de confidentialité ou nos pratiques en matière de protection des données, veuillez nous contacter à l’adresse dpo@systemin.fr.</p>
        </div>


@endsection
