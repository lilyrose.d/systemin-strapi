@extends('layout.app')

@section('content')

    <div class="container pt-5">
        <div class="container-fluid pt-5 container-background my-5">
            <div class="text-left">
                <h1 class="display-4 fw-bold">{{ $article['title'] }}</h1>
                @if(isset($article['createdAt']))
                    <p class="mt-5">devsys | {{ date('F d, Y', strtotime($article['createdAt'])) }}</p>
                @endif
            </div>
        </div>

        <div class="container pt-5 ">
            <div class="row">
                <div class="col">
                    @foreach ($paragraphs as $paragraph)
                        <h2 class="mb-5 mt-3 fw-bold">{!! ($paragraph['title']) !!}</h2>
                        <div class="paragraph">
                            @if(isset($paragraph['htmlContent']))
                                <p class="mb-5 mt-3">{!! $paragraph['htmlContent'] !!}</p>
                            @endif

                            @if (isset($paragraph['media']))
                                    <?php $media = $paragraph['media']; ?>
                                @if (isset($media['formats']['large']['url']))
                                        <?php $smallImageUrl = $strapiImageUrl . $media['formats']['large']['url']; ?>
                                    <div class="text-center pt-5">
                                        <img src="{{ $smallImageUrl }}" alt="{{ $media['name'] }}" class="img-fluid mb-5">
                                    </div>
                                @else
                                        <?php $imageUrl = $strapiImageUrl . $media['url']; ?>
                                    <div class="text-center">
                                        <img src="{{ $imageUrl }}" alt="{{ $media['name'] }}" class="img-fluid">
                                    </div>
                                @endif
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="container pt-5 mt-5">
            <div class="row justify-content-left">
                <h6>Partagez l'article</h6>
                <div class="col-auto">
                    <a href="https://twitter.com/intent/tweet?url={{ urlencode(request()->fullUrl()) }}" target="_blank">
                        <i class="bi bi-twitter fs-3 red-icon"></i>
                    </a>
                </div>
                <div class="col-auto">
                    <a href="https://www.linkedin.com/shareArticle?url={{ urlencode(request()->fullUrl()) }}" target="_blank">
                        <i class="bi bi-linkedin fs-3 red-icon"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection


