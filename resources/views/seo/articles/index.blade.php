@extends('layout.app')

@section('title', 'Articles Techniques - Systemin')

@section('content')

    <div class="container pt-5 mb-5 pb-5">
        <div class="container-fluid pt-5 my-5">
            <div class="text-center">
                <h1 class="display-2 fw-bold pb-5">Les Articles</h1>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-white">
        <div class="m-5">
            <form action="{{ route('seo.articles.index') }}" method="GET">
                <div class="form-group w-25">
                    <label class="pt-5 ms-1 mb-3" for="category">Filtrer par catégorie :</label>
                    <select class="form-control" id="category" name="category">
                        <option value="">Toutes les catégories</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category['id'] }}" @if($selectedCategory == $category['id']) selected @endif>{{ $category['attributes']['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-third mt-3 ">Filtrer</button>
            </form>
            <div class="row">
                @foreach ($articles as $article)
                    <div class="col-md-4">
                        <div class="thumbnail p-3 mb-4 bg-light shadow-lg rounded w-100" style="height: 300px;">
                            @if(isset($article['attributes']['title']))
                                <h4 class=" fw-bolder mt-3">{{ $article['attributes']['title'] }}</h4>
                            @endif
                            <!-- Afficher un extrait du premier paragraphe -->
                            @if(isset($article['attributes']['paragraphs']) && count($article['attributes']['paragraphs']) > 0)
                                @php
                                    $firstParagraph = $article['attributes']['paragraphs'][0]['content'];
                                @endphp
                                <p>{!! substr($firstParagraph, 0, 112) !!}...</p>
                            @endif
                            @if(isset($article['attributes']['slug']))
                                <a href="{{ route('seo.content.show', ['slug' => $article['attributes']['slug']]) }}" class="btn red">Voir plus</a>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
