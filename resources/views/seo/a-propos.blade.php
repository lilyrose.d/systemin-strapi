@extends('layout.app')

@section('content')

    <div class="container-fluid pt-5 mt-5 pb-5 ">
        <div class="container-fluid pt-5 ">
            <div class="text-center ms-0">
                <h1 class="display-2 fw-bold">À propos</h1>
                <p class="shorter-border mt-4"></p>
                <p class=" mt-4">Les profils de l’équipe fondatrice de Systemin</p>
            </div>
        </div>
    </div>


            <div class="container">
                <div class="container-fluid my-5">
                    <div class="row">
                        <div class="col-md-6 bg-white d-flex align-items-center">
                            <div class="text-left mx-auto">
                                <h1 class="display-4 fw-bold">Nicolas</h1>
                                <p class="font-weight-bold mt-5 grey-title">Nicolas, co-fondateur de Systemin, passionné d’informatique et d’automatisation, est guidé par une mission claire : aider les personnes confrontées à des tâches répétitives en collaborant étroitement avec les clients pour automatiser leur quotidien. Son approche est fondée sur l'écoute et la compréhension profonde des défis spécifiques de chaque projet. Nicolas prendra le temps de comprendre votre métier, vos besoins afin de vous apporter des réponses sur mesure pour améliorer votre quotidien au travail.</p>
                            </div>
                        </div>
                        <div class="col-md-6 bg-white d-flex align-items-center justify-content-end p-0">
                            <img src="{{asset("img/portraits/nicolas_cestari_dev_systemin-e1689605472655.jpg")}}" class="img-fluid" alt="Image Nicolas">
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="container-fluid my-5">
                    <div class="row">
                        <div class="col-md-6 bg-white d-flex align-items-center justify-content-start p-0">
                            <img src="{{asset("img/portraits/lilian_bellini_dev_systemin_bw-e1689605509998.jpeg")}}" class="img-fluid" alt="Image Lilian">
                        </div>
                        <div class="col-md-6 bg-white d-flex align-items-center">
                            <div class="text-left mx-auto">
                                <h1 class="display-4 fw-bold mt-5">Lilian</h1>
                                <p class="font-weight-bold mt-5 grey-title">Lilian, co-fondateur de Systemin, partage son expérience en ingénierie logicielle, notamment en
                                    développement web et mobile, visant à créer des solutions durables, robustes et maintenables. Son expérience chez Dassault, où il a travaillé sur
                                    des logiciels critiques embarqués pour jets privés, lui a inculqué l'importance d'une approche méthodique dans la gestion de projets, une philosophie qu'il adopte
                                    dans le développement d'applications. Son passage au Laboratoire Bordelais de Recherche en Informatique, où il s'est impliqué dans la recherche et la publication d'articles
                                    scientifiques, principalement axés sur l'optimisation algorithmique. Ces expériences lui ont permis de développer une approche réfléchie et innovante dans la résolution de problèmes complexes,
                                    une compétence qu'il met aujourd'hui au profit des projets de Systemin.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="container-fluid px-5 pb-5 ">
                <h2 class="py-5 red fw-bold">Pourquoi Systemin ?</h2>
                <p class="grey-title">Chez Systemin, nous avons fondé notre entreprise sur une conviction forte : l’automatisation transforme la façon dont les entreprises opèrent.  Notre passion pour l’informatique et ses enjeux
                    stratégiques majeurs a été le moteur de notre ambition : développer des solutions qui optimisent les tâches répétitives des entreprises.</p>
                <p class="grey-title">Notre mission est d’aider les entreprises à réussir leur transformation numérique en simplifiant leurs processus. Nous comprenons vos besoins, concevons des applications performantes, assurons leur sécurité
                    et leur maintenance. Notre approche directe garantit une communication efficace, et nos projets témoignent de notre engagement envers l’innovation. De la prise du nom de domaine au développement, en passant par l’hébergement et
                    les mises à jour, nous prenons en charge l’ensemble de votre informatique. </p>
                <p class="grey-title">Nous respectons la vie privée, favorisons l’accessibilité, et privilégions les technologies Open Source. Notre objectif est de rendre vos systèmes plus intelligents et efficaces pour que vous puissiez vous concentrer sur ce que vous faites de mieux.</p>
            </div>

            <div class="container-fluid px-5 pb-5 ">
                <h2 class="py-5 red fw-bold">Nos clients : TPE, PME et au-delà</h2>
                <p class="grey-title">Chez Systemin, nous comprenons les défis uniques des petites et moyennes entreprises qui cherchent à gagner en efficacité et en compétitivité. Nos clients sont des entreprises dynamiques, souvent confrontées à des tâches chronophages et répétitives.
                    Ces tâches peuvent inclure la gestion de commandes, des communications inter-logiciels complexes, ou encore le besoin d’uniformiser un système d’informations disparate.</p>
                <p class="grey-title">Notre mission est de transformer ces défis en opportunités. Nous automatisons ces processus fastidieux, aidant ainsi nos clients à s’adapter facilement à un environnement numérique efficace. L’automatisation remplace des activités manuelles par des solutions informatisées, libérant du temps pour des tâches à plus forte valeur ajouté.</p>
                <p class="grey-title">Mais notre expertise ne s’arrête pas là. Pour les entreprises souhaitant établir ou renouveler leur présence en ligne, nous offrons un service de création de sites web de A à Z. Notre équipe est suffisamment qualifiée pour gérer des projets complexes, ce qui signifie que même un « petit » site web est traité avec le plus haut niveau de compétence et de soin.</p>
                <p class="grey-title">En outre, en collaboration avec des designers talentueux, nous apportons à nos clients des designs responsives, modernes et créatifs, garantissant que leur présence en ligne est non seulement fonctionnelle mais aussi visuellement attrayante.</p>
                <p class="grey-title">Chez Systemin, nous sommes fiers de fournir des solutions qui répondent aux besoins spécifiques des TPE et PME, les aidant à se développer et à prospérer dans un monde numérique en constante évolution.</p>
            </div>

            <div class="container-fluid px-5 pb-5 ">
                <h2 class="py-5 red fw-bold">L’atout Systemin : Une approche client-centrée</h2>
                <p class="grey-title">Chez Systemin, nous nous positionnons d’abord en tant qu’entrepreneur, avec une approche unique axée sur l’écoute et la compréhension des projets de nos clients. Nous ne cherchons pas à imposer nos idées, mais plutôt à saisir en profondeur les besoins et les visions de ceux que nous servons. Notre processus commence par écouter attentivement, analyser les exigences, comprendre
                    les enjeux, confronter les idées et finalement restituer tout cela pour offrir la solution la plus adaptée à chaque projet.</p>
                <p class="grey-title">Les atouts de Systemin résident dans notre capacité à apprendre rapidement et notre motivation intrinsèque à relever de nouveaux défis. Notre équipe est caractérisée par sa polyvalence et sa persévérance, n’ayant pas peur d’explorer de nouvelles avenues ou de faire face à l’échec pour trouver la meilleure voie à suivre. Nous provenons d’horizons divers, ce qui enrichit notre perspective et notre compréhension des besoins des clients. Cette diversité nous permet
                    de nous mettre aisément à la place de nos clients, afin d’améliorer les flux de travail grâce à des logiciels sur mesure.</p>
                <p class="grey-title">Notre fort sens du service et notre aspiration à être un acteur de changement se traduisent par un engagement inébranlable envers nos clients. Chez Systemin, nous maintenons des normes de qualité élevées, tant sur le plan fonctionnel que du design. Nous nous engageons à fournir des solutions qui ne sont pas seulement performantes, mais aussi esthétiquement plaisantes, assurant ainsi une expérience utilisateur sans faute.</p>
                <p class="grey-title">En résumé, chez Systemin, nous sommes plus que des développeurs de solutions : nous sommes des partenaires engagés dans le succès de nos clients, offrant des services personnalisés qui répondent précisément à leurs besoins uniques.</p>
            </div>


@endsection
