@extends('layout.app')

@section('content')

    <div class="container pt-5 my-5 pb-5">
        <h1 class="text-center display-6 fw-bold text-center red pt-5">Application Web de Gestion de Remboursements de Réparations</h1>
    </div>

    <div class="container mx-auto px-5">
        <div class="summary-section ">
            <h5 class="fw-bold grey-title ">Résumé du projet</h5>
            <p class="article-text ">L’Application Web de Gestion de Remboursement de Réparation a été développée pour répondre aux
                besoins de la société iRep. Elle simplifie le processus de remboursement des réparations effectuées par des tiers intermédiaires
                tels qu’Ecologic et Ecosystem, offrant une solution efficace de centralisation des données pour les professionnels de la réparation de téléphones mobiles.</p>
                <a href="#images-section" class="btn btn-third " style="font-size: 20px">Découvrir le projet</a>
        </div>

        <div class="summary-section">
            <h2 class="fw-bold grey-title pt-5 mt-3 mb-5 text-center">Développement Technique</h2>
            <h5 class="fw-bold pb-3">Langages de programmation</h5>
            <p class="article-text">Pour assurer la robustesse et la sécurité de notre application, nous avons opté pour un ensemble de langages de programmation et de technologies robustes.
                Cela inclut l’utilisation du framework PHP Laravel, qui forme la base de notre application. Laravel offre une structure solide pour le développement web, permettant une gestion efficace
                de la logique applicative. Nous utilisons également les technologies front-end comme Bootstrap pour une interface utilisateur moderne et réactive, ainsi que MySQL comme système de gestion de base de données.
                L’ensemble de l’application est déployé sur des serveurs Linux pour garantir une stabilité optimale.</p>

            <h5 class="fw-bold pb-3 pt-3">Architecture</h5>
            <p class="article-text">L’architecture de notre application est conçue pour offrir une expérience utilisateur fluide et une gestion des données performante. Elle se compose d’une interface front-end intuitive qui facilite la navigation et l’interaction avec l’application.
                En parallèle, notre architecture back-end est robuste et s’appuie sur Laravel pour gérer efficacement les transactions avec les API d’Ecologic et d’Ecosystem. Cette structure assure une communication transparente entre les différentes composantes de l’application, garantissant
                ainsi une expérience utilisateur simplifiée.</p>
            <h5 class="fw-bold pb-3 pt-3">Sécurité et Performance</h5>
            <p class="article-text">Notre application assure la sécurité de vos données grâce à un système d’authentification. Ce système, appelé « Auth », protège les zones sensibles de l’application en ne permettant l’accès qu’aux utilisateurs autorisés.
                Pour garantir que seuls les utilisateurs légitimes ont accès aux données, nous utilisons des « jetons ». Ces jetons fonctionnent comme des clés d’accès spéciales et permettent à iRep de s’authentifier auprès des systèmes API d’Ecologic et d’Ecosystem en toute sécurité, renforçant ainsi la protection des informations.</p>
        </div>

        <div class="summary-section pt-5 mb-5">
            <h2 class="fw-bold grey-title mb-5 text-center ">Fonctionnalités et Utilisation</h2>
            <h5 class="fw-bold pb-3 pt-3">Description des Fonctionnalités Principales</h5>
            <p class="article-text">Notre application offre un ensemble de fonctionnalités essentielles pour simplifier la gestion des remboursements de réparation. Elle permet de soumettre, gérer et suivre les demandes de remboursement en toute simplicité. De plus, nous proposons une interface conviviale pour interagir avec les API d’Ecologic et d’Ecosystem, facilitant ainsi la communication avec ces organismes.</p>
        </div>

        <div class="summary-section">
            <h2 class="fw-bold grey-title pb-3 pt-3 text-center mb-5">Approche du Design</h2>
            <p class="article-text mb-5">Notre application adopte un design intuitif et épuré. Elle utilise Bootstrap de base pour une interface légère, simple à utiliser et efficace.
        </div>
        <div class="my-4 pb-5 text-center" data-aos="fade-up" data-aos-delay="150">
            <a href="{{route('seo.demande-devis')}}" class="btn btn-primary falling-button" data-button-id="2">Demande de devis</a>
        </div>

        <div id="images-section" class="container-fluid">
            <div class="row row-cols-1 row-cols-md-2 m-0">
                <div class="col ">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/irep/edit-ecologic.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/irep/edit-ecologic.png") }}" class="img-fluid" alt="Image 2">
                            <div class="image-text">
                                Edit Ecologic
                            </div>

                        </a>
                    </div>
                </div>

                <div class="col ">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/irep/ecosystem-edit.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/irep/ecosystem-edit.png") }}" class="img-fluid" alt="Image 2">
                            <div class="image-text">
                                Edit Ecosystem
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row row-cols-1 row-cols-md-2 m-0">
                <div class="col ">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/irep/login-irep.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/irep/login-irep.png") }}" class="img-fluid" alt="Image 2">
                            <div class="image-text">
                                Login
                            </div>

                        </a>
                    </div>
                </div>

                <div class="col ">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/irep/index-reparation.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/irep/index-reparation.png") }}" class="img-fluid" alt="Image 2">
                            <div class="image-text">
                                Index Ecosystem
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
