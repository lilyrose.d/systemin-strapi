@extends('layout.app')

@section('content')

    <div class="container pt-5 my-5 pb-5">
        <h1 class="text-center display-6 fw-bold text-center red pt-5">Application de Gestion de Biens Immobiliers</h1>
    </div>

    <div class="container mx-auto px-3">
        <div class="summary-section ">
            <h5 class="fw-bold grey-title ">Résumé du projet</h5>
            <p class="article-text ">Notre application, conçue pour l’agence immobilière Irragori, est une solution pratique de gestion de biens immobiliers,
                incluant des appartements, des maisons et des terrains. Elle a pour but de simplifier la gestion, la mise à jour et la publication des annonces immobilières,
                facilitant ainsi le travail quotidien des agents immobiliers et améliorant l’expérience des acheteurs potentiels.</p>
            <a href="#image-section" class="btn btn-third " style="font-size: 20px">Découvrir le projet</a>
        </div>

        <div class="summary-section">
            <h2 class="fw-bold grey-title pt-5 mt-3 mb-5 text-center">Développement Technique</h2>
            <h5 class="fw-bold pb-3">Environnement et Langages</h5>
            <p class="article-text">L’application est développée pour un environnement web, utilisant des technologies couramment adoptées dans le domaine. Le système d’exploitation Linux assure une base stable, tandis que Git & GitLab sont utilisés pour la gestion de versions, favorisant une bonne organisation du code.</p>
            <h5 class="fw-bold pb-3 pt-3">Développement Serveur et Base de Données</h5>
            <p class="article-text">Pour le côté serveur, nous avons choisi PHP, SQL et MySQL pour leur fiabilité et facilité d’utilisation. Le développement côté client est réalisé en HTML, CSS, Blade et Bootstrap, créant une interface utilisateur agréable et intuitive.</p>
            <h5 class="fw-bold pb-3 pt-3">Architecture de l’Application</h5>
            <p class="article-text">Notre application s’appuie sur une architecture MVC (Modèle-Vue-Contrôleur), avec des contrôleurs spécifiques pour gérer les différents types de propriétés. Les modèles incluent des propriétés, des appartements, des terrains, des maisons et des photos. Un système de références dynamique facilite la gestion des propriétés.</p>
            <h5 class="fw-bold pb-3 pt-3">Fonctionnalités Techniques</h5>
            <p class="article-text">L’envoi de mails automatique récupère les informations liées aux biens, et l’application propose des vues administratives et des vues clients pour la présentation des biens. Des paramétrages SEO de base sont intégrés pour améliorer la visibilité en ligne. De plus, un focus est mis sur les diagnostics énergétiques et les émissions de gaz à effet de serre.</p>
            <h5 class="fw-bold pb-3 pt-3">Sécurité et Performance</h5>
            <p class="article-text">Des validateurs de requêtes sont utilisés pour renforcer la sécurité, accompagnés de systèmes de login et de middleware d’authentification pour protéger les données et assurer une performance fiable. Pour assurer une robustesse du logiciel, les tests sont automatisés avec Dusk.</p>
        </div>

        <div class="summary-section pt-5 mb-5">
            <h2 class="fw-bold grey-title mb-5 text-center ">Fonctionnalités et Utilisation</h2>
            <h5 class="fw-bold pb-3 pt-3">Gestion des Biens</h5>
            <p class="article-text">L’application permet une gestion efficace des maisons, appartements et terrains (CRUD – Créer, Lire, Mettre à jour, Supprimer).</p>
            <h5 class="fw-bold pb-3 pt-3">Services Aux Clients</h5>
            <p class="article-text">Elle offre des fonctionnalités telles que la demande d’estimation et un formulaire de contact intelligent, qui associe les demandes des clients aux propriétés concernées.</p> </div>
        <div class="my-4 pb-5 text-center" data-aos="fade-up" data-aos-delay="150">
            <a href="{{route('seo.demande-devis')}}" class="btn btn-primary falling-button" data-button-id="2">Demande de devis</a>
        </div>

        <div id="image-section" class="container-fluid ">
            <div class="row row-cols-1 row-cols-md-4 m-0 ">
                <div class="col">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/irragori/irragori_client.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/irragori/irragori_client.png") }}" class="img-fluid" alt="Image 1">
                            <div class="image-text">Vue Client</div>
                        </a>
                    </div>
                </div>

                <div class="col">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/irragori/irragori_home.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/irragori/irragori_home.png") }}" class="img-fluid" alt="Image 2">
                            <div class="image-text">
                                Accueil
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col ">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/irragori/irragori_terrain.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/irragori/irragori_terrain.png") }}" class="img-fluid" alt="Image 2">
                            <div class="image-text">
                                Edit
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col ">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/irragori/irragori_estimation.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/irragori/irragori_estimation.png") }}" class="img-fluid" alt="Image 2">
                            <div class="image-text">
                                Estimation
                            </div>
                        </a>
                    </div>
                </div>

            </div>



            <div class="row row-cols-1 row-cols-md-2 m-0">
                <div class="col ">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/irragori/irragori_admin.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/irragori/irragori_admin.png") }}" class="img-fluid" alt="Image 2">
                            <div class="image-text">
                                Admin
                            </div>

                        </a>
                    </div>
                </div>

                <div class="col ">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/irragori/perf_web_irragori.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/irragori/perf_web_irragori.png") }}" class="img-fluid" alt="Image 2">
                            <div class="image-text">
                                Optimisation des performances
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
