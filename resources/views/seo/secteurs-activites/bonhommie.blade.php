@extends('layout.app')

@section('content')

    <div class="container pt-5 my-5 pb-5">
        <h1 class="text-center display-6 fw-bold text-center red pt-5">Application de Gestion de Fichiers de Tarifs pour Propriétés Viticoles</h1>
    </div>

    <div class="container mx-auto px-5">
        <div class="summary-section ">
            <h5 class="fw-bold grey-title ">Résumé du projet</h5>
            <p class="article-text ">Notre application innovante, spécialement conçue pour Bonhommie, une agence de distribution de vins et spiritueux basée à Blanquefort dans le bordelais, améliore la gestion des tarifs de propriétés viticoles. Destinée aux grossistes, cavistes, et restaurateurs, elle facilite l’accès aux informations tarifaires essentielles, permettant une expérience utilisateur optimisée et une gestion des données simplifiée.

                Cette application représente une solution complète pour la gestion des informations tarifaires dans le secteur viticole, offrant à Bonhommie un avantage concurrentiel significatif grâce à son efficacité et à sa facilité d’utilisation.</p>
            <a href="#images-section" class="btn btn-third " style="font-size: 20px">Découvrir le projet</a>
        </div>

        <div class="summary-section">
            <h2 class="fw-bold grey-title pt-5 mt-3 mb-5 text-center">Développement Technique</h2>
            <h5 class="fw-bold pb-3">Environnement et Langages</h5>
            <p class="article-text">Développée pour un environnement web, notre application supporte une large gamme d’appareils mobiles et s’intègre facilement aux systèmes logiciels existants. Utilisant Linux comme système d’exploitation, notre choix s’est porté sur Git & GitLab pour la gestion de versions, assurant ainsi une collaboration et une intégrité du code optimales.</p>

            <h5 class="fw-bold pb-3 pt-3">Développement Serveur et Base de Données</h5>
            <p class="article-text">Nous avons employé PHP, SQL et MySQL pour le développement côté serveur, garantissant robustesse et fiabilité. Le développement côté client a été réalisé avec HTML, CSS, Blade et Bootstrap, offrant une interface utilisateur élégante et réactive.</p>

            <h5 class="fw-bold pb-3 pt-3">Architecture de l’Application</h5>
            <p class="article-text">L’application est structurée autour de modèles de gestion pour les clients, les propriétés, les départements de ventes et les régions, avec une gestion complexe des relations entre ces entités (1 – 1, 1 – n, n – n). Elle comprend 13 tables et migrations, assurant une organisation et une évolutivité des données.</p>

            <h5 class="fw-bold pb-3 pt-3">Sécurité et Performance</h5>
            <p class="article-text">La sécurité est renforcée par des vérifications rigoureuses des droits d’accès, l’utilisation de middleware pour authentifier différents types d’utilisateurs, et une synchronisation continue entre l’espace de stockage et la base de données.</p>
        </div>

        <div class="summary-section pt-5 mb-5">
            <h2 class="fw-bold grey-title mb-5 text-center ">Fonctionnalités et Utilisation</h2>
            <h5 class="fw-bold pb-3 pt-3">Gestion complète (CRUD) des propriétés, clients et des tarifs</h5>
            <p class="article-text">L’acronyme CRUD représente les quatre fonctions de base de la gestion persistante de données, à savoir :
            <ul class="article-list">
                <li class="article-list-item">Create (Créer) : la capacité de créer de nouvelles entrées.</li>
                <li class="article-list-item">Read (Lire) : la capacité de lire ou de récupérer des informations.</li>
                <li class="article-list-item">Update (Mettre à jour) : la capacité de modifier les informations existantes.</li>
                <li class="article-list-item">Delete (Supprimer) : la capacité de supprimer des informations.</li>
            </ul>

            <h5 class="fw-bold pb-3 pt-3">Liens et Téléchargements</h5>
            <p class="article-text">Elle offre la possibilité de lier un client (grossiste, caviste, restaurant) à un département de vente spécifique et permet aux clients de télécharger facilement des tarifs et des fiches techniques.</p>
        </div>

        <div class="summary-section">
            <h2 class="fw-bold grey-title pb-3 pt-3 text-center mb-5">Approche du Design</h2>
            <p class="article-text mb-5">Notre conception visuelle s’inspire des dernières tendances du secteur viticole, reflétant l’image de marque de Bonhommie ainsi que l’élégance des grandes propriétés viticoles du bordelais. Cette inspiration se manifeste dans notre choix de couleurs, de types de bordures et de polices, créant une esthétique à la fois moderne et en harmonie avec l’univers du vin. L’interface de l’application se distingue par son design intuitif et épuré, facilitant l’expérience utilisateur et mettant en valeur le contenu de manière élégante.</p>

            <h2 class="fw-bold grey-title pb-3 pt-3 text-center mb-5">Adaptabilité sur Différents Appareils</h2>
            <p class="article-text">L’adaptabilité est au cœur de notre développement. Notre objectif premier a été de garantir que l’application soit parfaitement utilisable sur tous les appareils – ordinateurs, tablettes et smartphones – tant pour les clients que pour les administrateurs. Pour atteindre ce but, nous avons mené une série de tests rigoureux et appliqué un haut niveau d’exigence à chaque étape du développement. Le résultat est une application réactive, qui offre une expérience utilisateur fluide et cohérente sur toutes les tailles d’écran, assurant ainsi une accessibilité maximale et une utilisation sans contrainte quel que soit le dispositif choisi.</p>
        </div>
        <div class="my-4 pb-5 text-center" data-aos="fade-up" data-aos-delay="150">
            <a href="{{route('seo.demande-devis')}}" class="btn btn-primary falling-button" data-button-id="2">Demande de devis</a>
        </div>


        <div id="images-section" class="container-fluid ">
            <div class="row row-cols-1 row-cols-md-4 m-0 ">
                <div class="col">
                        <div class="image-container position-relative">
                            <a href="{{ asset("img/secteurs-activites/bonhommie/bonhommie_prices.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/bonhommie/bonhommie_prices.png") }}" class="img-fluid" alt="Image 1">
                            <div class="image-text">Tarifs</div>
                            </a>
                        </div>
                </div>
                <div class="col">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/bonhommie/bonhommie_login.png") }}" data-lightbox="images">
                        <img src="{{ asset("img/secteurs-activites/bonhommie/bonhommie_login.png") }}" class="img-fluid" alt="Image 2">
                        <div class="image-text">
                            Connexion
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col ">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/bonhommie/bonhommie_type.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/bonhommie/bonhommie_type.png") }}" class="img-fluid" alt="Image 2">
                            <div class="image-text">
                                Type Client
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col ">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/bonhommie/bonhommie_client.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/bonhommie/bonhommie_client.png") }}" class="img-fluid" alt="Image 2">
                            <div class="image-text">
                                Vue client
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row row-cols-1 row-cols-md-3 m-0">
                    <div class="col ">
                        <div class="image-container position-relative">
                            <a href="{{ asset("img/secteurs-activites/bonhommie/bonhommie_update.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/bonhommie/bonhommie_update.png") }}" class="img-fluid" alt="Image 2">
                            <div class="image-text">
                                Update
                            </div>
                            </a>
                        </div>
                    </div>

                <div class="col ">
                    <div class="image-container position-relative">
                        <a href="{{ asset("img/secteurs-activites/bonhommie/bonhommie_property.png") }}" data-lightbox="images">
                            <img src="{{ asset("img/secteurs-activites/bonhommie/bonhommie_property.png") }}" class="img-fluid" alt="Image 2">
                            <div class="image-text">
                                Gestion propriété
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col ">
                    <div class="image-container position-relative">
                        <img src="{{ asset("img/secteurs-activites/bonhommie/bonhommie_home.png") }}" class="img-fluid" alt="Image 2">
                        <div class="image-text">
                            Accueil
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>



@endsection
