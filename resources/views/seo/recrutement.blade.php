@extends('layout.app')

@section('content')

<div class="container">
    <div class="container-fluid pt-5 my-5 pb-5">
        <h1 class="text-center display-6 fw-bold text-center grey-title pt-5">Recrutement</h1>
        <p class=" mt-5 article-text text-center m-5">Rejoignez l’aventure Systemin et contribuez activement au développement
            d’une entreprise innovante dans le secteur informatique. Nous cherchons des gens passionnés, prêts à relever des défis
            et à apporter leur pierre à l’édifice dans un environnement dynamique et en constante évolution. Découvrez nos offres et commencez votre parcours
            chez Systemin, où chaque contribution a un impact significatif sur notre succès commun.</p>
    </div>

    <div class="container-fluid">
    <h3 class=" text-center fw-bold  grey-title pt-5 pb-5">Candidature spontanée</h3>
        <p class=" text-left ms-4 me-4">Nous vous remercions vivement pour l’intérêt que vous portez à notre entreprise.
            Actuellement, nous n’avons pas de postes ouverts pour de nouveaux recrutements. Cependant,
            nous encourageons vivement les candidatures spontanées. Si vous pensez avoir les compétences et
            l’expérience qui pourraient bénéficier à notre équipe, n’hésitez pas à nous envoyer votre candidature.
            Nous conserverons votre CV et vos informations dans notre base de données pour toute opportunité future correspondant à votre profil.
            Encore une fois, merci pour l’intérêt que vous portez à notre entreprise et nous vous souhaitons le meilleur dans vos recherches professionnelles.</p>
        <a href="#" class="btn btn-grey mb-5 ms-4 " >Aucune offre</a>
    </div>
</div>
@endsection
