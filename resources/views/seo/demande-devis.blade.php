@extends('layout.app')

@section('content')

    <div class="container pt-5 mb-5 pb-5">
        <div class="container-fluid pt-5 container-background my-5">
            <div class="text-center">
                <h1 class="display-2 fw-bold">Demande de Devis</h1>
                <p class="shorter-border mt-4"></p>
                <p class="font-weight-bold me-3 mt-4" >C'est le moment de lancer votre projet!</p>
            </div>
        </div>
    </div>

    <div class="container-fluid pt-5 px-md-5 pb-5 bg-white mx-auto mb-5">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-8">
                    <p class="text-center mb-5">Remplissez le formulaire ci-dessous pour obtenir un devis pour votre projet. Si possible, ajoutez votre cahier des charges au format .pdf en pièce jointe.</p>
                    <form action="{{ route('seo.send-devis') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 mt-3">
                                <div class="form-group">
                                    <label for="name" class="visually-hidden">Nom</label>
                                    <input type="text" id="name" name="name" required class="form-control" placeholder="Nom">
                                </div>
                            </div>
                            <div class="col-md-6 mt-3">
                                <div class="form-group">
                                    <label for="email" class="visually-hidden">Email</label>
                                    <input type="email" id="email" name="email" required class="form-control" placeholder="Email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mt-3">
                                <div class="form-group">
                                    <label for="phone" class="visually-hidden">Téléphone</label>
                                    <input type="tel" id="phone" name="phone" class="form-control" placeholder="Téléphone">
                                </div>
                            </div>
                            <div class="col-md-6 mt-3">
                                <div class="form-group">
                                    <label for="service" class="visually-hidden">Services</label>
                                    <select id="service" name="service" class="form-control">
                                        @foreach($services['data'] as $service)
                                            <option value="{{ $service['attributes']['title'] }}" @if(request()->has('service') && request('service') == $service['attributes']['title']) selected @endif>{{ $service['attributes']['title'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="form-group mt-4">
                            <label for="message">Message</label>
                            <textarea id="message" name="message" required class="form-control mt-1" style="resize: none;" ></textarea>
                        </div>
                        <div class="form-group mt-5">
                            <label for="file">Cahier des charges</label>
                            <input type="file" id="file" name="file" class="form-control-file">
                        </div>
                        <div class="form-group mt-3">
                            <input type="submit" value="Envoyer" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>


@endsection
