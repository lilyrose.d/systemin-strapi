@extends('layout.app')

@section('content')

<div class="container ">
    <div class="container-fluid pt-5 container-background my-5 ">
        <div class="row">
            <div class="col-md-6 d-flex align-items-center">
                <div class="text-left">
                    <h1 class="display-4 fw-bold">Maintenance et Infogérance</h1>
                    <p class="font-weight-bold mt-5 me-5">Systemin vous propose des services de maintenance et d’infogérance de haut niveau pour assurer la pleine accessibilité et fonctionnalité de vos systèmes informatiques.</p>
                </div>
            </div>
            <div class="col-md-6">
                <img src="{{asset("img/back/inforgerance_systemin-e1690447917268.jpeg")}}" class="d-block img-fluid" alt="Image de Bonhommie">
            </div>
        </div>
    </div>
</div>

<div class="container-fluid pt-5 pb-5 bg-white ">
    <h1 class="text-center display-4 fw-bold pb-5 mt-5">Forfaits de maintenance</h1>
    <div class="row justify-content-center">
        <div class="col-md-3 position-relative mt-5">
            <div class="bg-whitesmoke p-4 rounded forfait-background">
                <div class="label-banner  align-content-end">Populaire</div>
                <h2 class="text-center red fw-bold">Essentiel</h2>
                <h6 class="text-center fw-bold grey pb-3 pt-3">Infogérance basique</h6>
                <h1 class="text-center display-5">50€</h1>
                <p class="text-center pb-3">Mensuellement</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-hdd-stack-fill"></i> Gestion de l'hébergement et du domaine</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-database-fill"></i> Sauvegarde intégrale quotidienne</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-tools"></i> Maintenance corrective</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-robot"></i> Protection anti-bot</p>
                <p class="text-center custom-border-bottom"><i class="fas fa-envelope"></i> Support technique email</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-calendar-check"></i> Délais d'intervention d'une semaine (SLA 7 jours)</p>
                <p class="text-center"><i class="bi bi-briefcase"></i> 10 heures d'assistance technique incluses par an</p>
            </div>
        </div>
        <div class="col-md-3 mt-5">
            <div class="bg-whitesmoke p-4 rounded forfait-background">
                <h2 class="text-center red fw-bold">Premium</h2>
                <h6 class="text-center fw-bold grey pb-3 pt-3">Infogérance avancée</h6>
                <h1 class="text-center display-5">125€</h1>
                <p class="text-center pb-3">Mensuellement</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-hdd-stack-fill"></i> Gestion de l'hébergement et du domaine</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-database-fill"></i> Sauvegarde intégrale quotidienne</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-diagram-3-fill"></i> Réplication géographique</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-tools"></i> Maintenance corrective et évolutive</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-robot"></i> Protection anti-bot</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-send"></i> Support technique email</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-calendar-check"></i> Délais d'intervention de moins de trois jours (SLA 56 heures maximum)</p>
                <p class="text-center"><i class="bi bi-briefcase"></i> 26 heures d'assistance technique incluses par an</p>
            </div>
        </div>
        <div class="col-md-3 mt-5">
            <div class="bg-whitesmoke p-4 rounded forfait-background">
                <h2 class="text-center red fw-bold">Entreprise</h2>
                <h6 class="text-center fw-bold grey pb-3 pt-3">Infogérance complète</h6>
                <h1 class="text-center display-5">300€</h1>
                <p class="text-center pb-3">Mensuellement</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-hdd-stack-fill"></i> Gestion de l'hébergement et du domaine</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-database-fill"></i> Une offre de sauvegarde sur-mesure</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-diagram-3-fill"></i> Réplication géographique</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-tools"></i> Maintenance corrective et évolutive</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-robot"></i> Protection anti-bot</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-send"></i> Support technique email</p>
                <p class="text-center custom-border-bottom"><i class="bi bi-calendar-check"></i> Délais d'intervention de moins d'un jour (SLA 24 heures maximum)</p>
                <p class="text-center"><i class="bi bi-briefcase"></i> 63 heures d'assistance technique incluses par an</p>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid pt-5 pb-5 bg-white">
    <div class="ms-lg-5 ms-sm-auto">
        <h3 class="red mb-5 ms-lg-5">Détail des services proposés dans les forfaits de maintenance</h3>
        <ul class="ms-lg-5 ms-sm-auto">
            <li>
                Gestion de l’hébergement et du domaine :
                <ul class="service-list-filled mt-2">
                    <li>Assure la gestion complète de l’hébergement et du domaine.</li>
                    <li>Veille à la sécurité et à l’opérationnalité constants.</li>
                </ul>
            </li>
            <li class="mt-3">
                Sauvegardes quotidiennes :
                <ul class="service-list-filled mt-2">
                    <li>Effectue une sauvegarde complète des données chaque jour.</li>
                    <li>Garantit la récupération rapide en cas de perte.</li>
                </ul>
            </li>
            <li class="mt-3">
                Réplication géographique :
                <ul class="service-list-filled mt-2">
                    <li>Duplique les données dans plusieurs emplacements géographiquement éloignés.</li>
                    <li>Augmente la sécurité en minimisant les risques de perte de données.</li>
                </ul>
            </li>
            <li class="mt-3">
                Maintenance corrective :
                <ul class="service-list-filled mt-2">
                    <li>Veille au bon fonctionnement de l’application.</li>
                    <li>Effectue les mises à jour et corrections nécessaires.</li>
                </ul>
            </li>
            <li class="mt-3">
                Maintenance évolutive :
                <ul class="service-list-filled mt-2">
                    <li>Comprend la maintenance basique.</li>
                    <li>Effectue des évolutions cosmétiques et fonctionnelles mineures.</li>
                </ul>
            </li>
            <li class="mt-3">
                Offre de sauvegarde sur-mesure :
                <ul class="service-list-filled mt-2">
                    <li>Met en place une solution de sauvegarde adaptée aux besoins spécifiques du client.</li>
                </ul>
            </li>
            <li class="mt-3">
                Délais d’intervention :
                <ul class="service-list-filled mt-2">
                    <li>Engagement à intervenir dans un délai spécifié en cas de problème (varie selon le forfait).</li>
                </ul>
            </li>
            <li class="mt-3">
                Assistance :
                <ul class="service-list-filled mt-2">
                    <li>Fournit une assistance dédiée pendant une durée spécifiée (varie selon le forfait).</li>
                </ul>
            </li>
        </ul>
    </div>
</div>







@endsection
