@extends('layout.app')

@section('content')

    <div class="container-fluid pt-5 mt-5 pb-5">
            <div class="text-center ms-5 me-5">
                <h2 class="red fw-bold">Conditions générales de vente pour la prestation de services de Systemin</h2>
            </div>
    </div>

        <div class="container-fluid w-75">
            <p>Entrées en vigueur le : 15/04/2023.</p>
            <h6>Préambule</h6>
            <p>Les présentes Conditions Générales de Vente (ci-après « Conditions ») s’appliquent sans restriction ni réserves à toutes les prestations de services proposées par Systemin (ci-après le « Prestataire ») sur son site Web. Elles définissent les droits et obligations du Prestataire et du Client dans le cadre de la vente des services.</p>
            <h6>Article 1 – Présentation du Prestataire</h6>
            <p>Systemin, entreprise individuelle représentée par Nicolas CESTARI, avec siège social au 8 rue Edouard Lalo 33520 BRUGES peut être contactée par email à nico[at]systemin.fr.</p>
            <h6>Article 2 – Services Offerts</h6>
            <h7>2.1 Description des Services :</h7>
            <p>Systemin propose les services suivants :</p>
            <ul>
                <li>Développement application Web</li>
                <li>Développement et intégration d’API</li>
                <li>Développement application mobile</li>
                <li>Développement logiciel</li>
                <li>Création site internet</li>
                <li>Maintenance et infogérance</li>
            </ul>
            <p>Et toutes prestations s’y rattachant</p>
            <h7>2.2 Personnalisation :</h7>
            <p>Des services sur mesure peuvent être proposés en fonction des besoins spécifiques du client.</p>
            <h6>Article 3 – Processus de Commande</h6>
            <h7>3.1 Demande de Service :</h7>
            <p>Le Client peut demander un service via le site Web, par téléphone ou email. Un devis sera fourni sur la base des besoins du client.</p>
            <h7>3.2 Acceptation de l’Offre :</h7>
            <p>La commande est considérée comme acceptée après validation du devis par le client et paiement de l’acompte si applicable.</p>
            <h6>Article 4 – Tarifs et Paiement</h6>
            <h7>4.1 Prix :</h7>
            <p>Les prix des services sont indiqués en euros, hors taxes. Ils sont susceptibles de modification mais les tarifs applicables sont ceux en vigueur au moment de la commande.</p>
            <h7>4.2 Modalités de Paiement :</h7>
            <p>Les paiements peuvent être effectués par virement bancaire, carte de crédit ou selon les modalités définies dans le devis.</p>
            <p>Pénalités de Retard de Paiement</p>
            <p>Conformément aux conditions générales de vente et aux dispositions légales en vigueur, tout retard de paiement entraînera l’application de pénalités de retard. Ces pénalités sont calculées sur la base du taux d’intérêt légal en vigueur au moment du retard, augmenté de 10 points de pourcentage.</p>
            <p>Les pénalités de retard sont exigibles sans qu’un rappel soit nécessaire et seront calculées à partir du jour suivant la date de règlement figurant sur la facture jusqu’à la date de paiement effectif. Le montant des pénalités sera automatiquement et de plein droit acquis à notre entreprise, sans préjudice de toute autre action que nous serions en droit d’intenter, le cas échéant, à l’encontre du client en défaut de paiement.</p>
            <p>En plus des pénalités de retard, une indemnité forfaitaire pour frais de recouvrement d’un montant de 40 euros sera due, conformément à l’article L. 441-10 du code de commerce.</p>
            <p>Nous vous rappelons que le délai de paiement est fixé, par principe, au 30e jour suivant l’exécution de la prestation de service.</p>
            <h6>Article 5 – Exécution des Services</h6>
            <h7>5.1 Délais :</h7>
            <p>Les délais d’exécution des services sont indiqués dans le devis. Ces délais sont indicatifs et peuvent varier en fonction de la complexité du service demandé.</p>
            <h7>5.2 Obligations du Client :</h7>
            <p>Le Client s’engage à fournir toutes les informations et documents nécessaires à la bonne exécution du service.</p>
            <h6>Article 6 – Responsabilité et Garantie</h6>
            <h7>6.1 Qualité des Services :</h7>
            <p>Systemin s’engage à fournir des services conformes aux attentes définies dans le devis.</p>
            <h7>6.2 Limitation de Responsabilité :</h7>
            <p>La responsabilité de Systemin ne peut être engagée en cas de retard ou de défaut d’exécution dû à une cause extérieure.</p>
            <h6>Article 7 – Droit de Rétractation</h6>
            <p>Conformément à la législation en vigueur, le droit de rétractation n’est pas applicable aux contrats de prestation de services entièrement exécutés avant la fin du délai de rétractation avec l’accord du client.</p>
            <h6>Article 8 – Résolution du Contrat</h6>
            <p>Le contrat peut être résolu par l’une ou l’autre des parties en cas de manquement grave aux obligations stipulées dans ces Conditions ou dans le devis.</p>
            <h6>Article 9 – Confidentialité</h6>
            <p>Systemin s’engage à respecter la confidentialité des informations fournies par le client dans le cadre de l’exécution des services.</p>
            <h6>Article 10 – Droit Applicable et Litiges</h6>
            <p>En cas de litige, une tentative de résolution à l’amiable sera privilégiée. À défaut, le litige sera soumis aux tribunaux compétents.</p>
            <h6>Conditions de garantie des sites Web</h6>
            <p>1. Période de Garantie de 30 Jours :</p>
            <p>Après la mise en ligne du site web, vous bénéficiez d’une période de 30 jours durant laquelle les modifications mineures et corrections fonctionnelles sont gratuites. Cette période commence à partir de la date de mise en ligne officielle du site.</p>
            <p>2. Tarification après la Période de Garantie :</p>
            <p>– Modifications mineures : 50 euros par modification mineure (texte, image).</p>
            <p>– Intervention d’une demi-journée : 220 euros.</p>
            <p>3. Évolutions fonctionnelles : Toute évolution fonctionnelle après la période de garantie de 30 jours sera soumise à un devis spécifique, en fonction de la complexité et de la portée des modifications demandées. est la même mise en page que ça</p>
        </div>



@endsection
