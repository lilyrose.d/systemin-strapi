@extends('layout.app')

@section('title', 'Spécialiste en développement applicatif à Bordeaux - Systemin')

@section('content')

    <div class="container pt-5 pb-5 mb-5">
        <div class="row justify-content-center">
            <!-- Colonne 1 -->
            <div class="col-md-6">
                <div class="text-left pt-5 ps-md-5 me-4">
                    <p class="text_title pt-5">S Y S T E M I N</p>
                    <h1 class="display-6">Des applications, sites Web, & API sur mesure pour toutes les tailles d'entreprises.</h1>
                    <p class="text_subtitle fw-light mt-4 mb-4">Chez Systemin, nous allions l'art du design à la science de la programmation pour créer des sites web et applications qui sont aussi beaux que performants. Simplifiez votre activité avec des solutions sur mesure, conçues pour améliorer chaque aspect de votre entreprise.</p>
                    <div class="bg-white rounded border-white pe-5 form-container pb-3" data-aos="fade-up" data-aos-delay="150" style="box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.2), 0px 0px 4px rgba(0, 0, 0, 0.08);">
                        <h5 class="ms-4 pt-3">Demande de devis express</h5>
                        <p class="ms-4 fw-light">Envie de concrétiser un projet rapidement ?</p>
                        <form action="{{ route('seo.send-devis') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group mt-4 row justify-content-center">
                                <div class="col-lg-4 mb-3 mb-lg-0">
                                    <label for="name" class="visually-hidden">Prénom Nom</label>
                                    <input type="text" id="name" name="name" required class="form-control form-control-sm" placeholder="Prénom Nom">
                                </div>
                                <div class="col-lg-3 mb-3 mb-lg-0">
                                    <label for="phone" class="visually-hidden">Téléphone</label>
                                    <input type="tel" id="phone" name="phone" class="form-control form-control-sm" placeholder="Téléphone">
                                </div>
                                <div class="col-lg-4">
                                    <label for="email" class="visually-hidden">Email</label>
                                    <input type="email" id="email" name="email" required class="form-control form-control-sm" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group mt-3 row justify-content-center">
                                <div class="col-sm-5 mb-3">
                                    <label for="message" class="visually-hidden">Résumé de votre projet</label>
                                    <textarea id="message" name="message" required class="form-control form-control-sm" style="width: 100%; height: 2rem; resize: none;" placeholder="Résumé de votre projet"></textarea>
                                </div>
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <label for="file" class="visually-hidden">Cahier des charges</label>
                                    <div class="input-group">
                                        <label for="file" class="input-group-text custom-label bg-white"><i class="bi bi-file-earmark-text"></i>Cahier des charges</label>
                                        <input type="file" id="file" name="file" class="form-control-file visually-hidden form-control-sm">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <button type="submit" class="btn btn-formaulaire scale-on-hover" style=" box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.2), 0px 0px 4px rgba(0, 0, 0, 0.08);">
                                        Envoyer<i class="bi bi-arrow-right"></i>
                                    </button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Colonne 2 -->
            <div class="col-md-6 mt-5">
                <div class="row mt-5 ps-md-5 ms-5-md-5">
                    <div class="col-md-12 mt-5 ms-md-5 ml-md-5">
                        <div class="card border-left mb-4 bg-white rounded scale-on-hover" style="max-width: 30rem; height: 7em">
                            <div class="card-body d-flex justify-content-between align-items-center">
                                <div>
                                    <h6 class="card-title">Création de site internet</h6>
                                    <p class="card-text fw-light w-75">Lorem ipsum dolor sit amet, consectetur adipiscing elit..</p>
                                </div>
                                <a href="{{ route('seo.content.show', ['slug' => $services['data'][4]['attributes']['slug']]) }}" class="btn bi bi-arrow-right red-icon " data-button-id="2"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 ms-md-5 mt-3 ml-md-5">
                        <div class="card border-left mb-4 bg-white rounded scale-on-hover" style="max-width: 30rem; height: 7em">
                            <div class="card-body d-flex justify-content-between align-items-center">
                                <div>
                                    <h6 class="card-title">Développement et intégration d'API</h6>
                                    <p class="card-text text_title fw-light w-75">Lorem ipsum dolor sit amet, consectetur adipiscing elit..</p>
                                </div>
                                <a href="{{ route('seo.content.show', ['slug' => $services['data'][1]['attributes']['slug']]) }}" class="btn bi bi-arrow-right red-icon" data-button-id="2"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 ms-md-5 mt-3 ml-md-5">
                        <div class="card border-left mb-5 bg-white rounded scale-on-hover" style="max-width: 30rem; height: 7em">
                            <div class="card-body d-flex justify-content-between align-items-center">
                                <div>
                                    <h6 class="card-title">Maintenance & infogérance</h6>
                                    <p class="card-text text_title fw-light w-75  ">Lorem ipsum dolor sit amet, consectetur adipiscing elit..</p>
                                </div>
                                <a href="{{ route('seo.services.maintenance-et-infogerance') }}" class="btn bi bi-arrow-right red-icon scale-on-hover" data-button-id="2"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid mt-5 bg-white p-3 p-md-5 position-relative">
        <div class="extra-horizontal-margin pt-3 row justify-content-center">
            <div class="col-4 col-md-1 mx-3">
                <img src="{{ asset("img/entreprises/parisnanterre.png") }}" class="d-block img-fluid mx-auto mb-3" style="max-height: 40px;" alt="Image de Paris Nanterre">
            </div>
            <div class="col-4 col-md-1 mx-2">
                <img src="{{ asset("img/entreprises/irep.png") }}" class="d-block img-fluid mx-auto mb-3" style="max-height: 40px;" alt="Image de IREP">
            </div>
            <div class="col-4 col-md-1 mx-2">
                <img src="{{ asset("img/entreprises/reaut.png") }}" class="d-block img-fluid mx-auto mb-3" style="max-height: 40px;" alt="Image de reaut">
            </div>
            <div class="col-4 col-md-1 mx-2">
                <img src="{{ asset("img/entreprises/bonhommie.png") }}" class="d-block img-fluid mx-auto mb-3" style="max-height: 40px;" alt="Image de Bonhommie">
            </div>
            <div class="col-4 col-md-1 mx-2">
                <img src="{{ asset("img/entreprises/t4ex-1.png") }}" class="d-block img-fluid mx-auto mb-3" style="max-height: 40px;" alt="Image t4EX">
            </div>
            <div class="col-4 col-md-1 mx-2">
                <img src="{{ asset("img/entreprises/mcs.png") }}" class="d-block img-fluid mx-auto" style="max-height: 40px;" alt="Image NKAMA GYUDON">
            </div>
        </div>
        <div class="position-absolute top-50 end-0 translate-middle-y d-none d-md-block" style="z-index: 1;">
            <img src="{{asset("img/back/Frame_6bonhmmie 1.png")}}" class="img-fluid" style="max-width: 100%;" alt="Bonhommie">
        </div>
        <div class="container mt-3">
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-6 rounded bg-whitesmoke p-3 mt-5 w-75 w-md-auto pb-5">
                    <div class="text-left ms-5">
                        <h1 class="display-6 w-75 mt-4">Sécurité et performance sans soucis : confiez la maintenance de votre site à votre spécialiste dédié.</h1>
                        <p class="text_subtitle fw-light mt-4 w-100 w-md-50">Hébergement, sauvegardes quotidiennes, et support technique : tout inclus pour une tranquillité d'esprit.</p>
                        <a href="{{ route('seo.services.maintenance-et-infogerance') }}" class="btn btn-primary scale-on-hover " data-button-id="2" style="box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.2), 0px 0px 4px rgba(0, 0, 0, 0.08);">Découvrir les offres de maintenance<i class="bi bi-arrow-right"></i> </a>
                    </div>
                </div>
            </div>
        </div>
    </div>











    <div class="container mt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <h1 class="display-6 mt-5 mb-5">Plus qu'une <span style="color: #EC1D25;">équipe de développement</span> : votre partenaire en innovation et succès</h1>
            </div>
        </div>

        <div class="container mt-5 pb-5">
            <div class="row justify-content-center  align-items-center">
                <div class="col-md-5 mt-4 ">
                    <img src="img/back/terminal.png" alt="Terminal" class="img-fluid rounded">
                </div>

                <div class="col-md-7 mt-4">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card-avantages-transparent grey bg-white rounded  p-4 " style="box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.2), 0px 0px 4px rgba(0, 0, 0, 0.08);">
                                <div class="card-body d-flex flex-column text-left" >
                                    <i class="bi bi-chat-left-dots card-icon red-icon" style="font-size:40px;"></i>
                                    <h6 class="card-title mb-2">Solutions personnalisées</h6>
                                    <p class="text_subtitle card-text w-75">Bénéficiez de solutions sur mesure, conçues pour répondre à vos exigences spécifiques.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card-avantages-transparent grey bg-white rounded p-4 ms-3 " style="box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.2), 0px 0px 4px rgba(0, 0, 0, 0.08);">
                                <div class="card-body d-flex flex-column text-left" >
                                    <i class="bi bi-terminal card-icon red-icon" style="font-size:40px;"></i>
                                    <h6 class="card-title mb-2">Programmation de qualité</h6>
                                    <p class="text_subtitle card-text">Conception et programmation d'applications et de sites web esthétiquement attrayants et robustes techniquement.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card-avantages-transparent grey bg-white rounded p-3 mt-5" style="box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.2), 0px 0px 4px rgba(0, 0, 0, 0.08);">
                                <div class="card-body d-flex flex-column text-left">
                                    <i class="bi bi-keyboard card-icon red-icon" style="font-size:40px;"></i>
                                    <h6 class="card-title mb-2">Support technique fiable</h6>
                                    <p class="text_subtitle card-text">Nous offrons un support technique pour garantir que vos applications et logiciels fonctionnent de manière optimale.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card-avantages-transparent grey bg-white rounded p-3 mt-5 ms-3" style="box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.2), 0px 0px 4px rgba(0, 0, 0, 0.08);">
                                <div class="card-body d-flex flex-column text-left" >
                                    <i class="bi bi-gear card-icon red-icon" style="font-size:40px;"></i>
                                    <h6 class="card-title mb-2">Une exécution efficace</h6>
                                    <p class="text_subtitle card-text w-75">Garantir que vos projets sont livrés dans les délais impartis, sans compromis sur la qualité.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="container-fluid mt-5 bg-white py-5 px-5 custom-background mb-5 rounded">
        <h1 class=" text-center display-4 fw-bold mb-5">Technologies</h1>
        <p class="text-center"><small>Nous utilisons ces technologies pour façonner vos services web. Découvrez notre boîte à outils ci-dessous.</small></p>
        <div class="row justify-content-center">
            <div class="col-sm-10 col-md-8 col-lg-6 text-center">
                <img src="{{asset('img/back/technologies_web_systemin.png')}}" class="img-fluid mx-auto pb-5" style="max-width: 100%; margin-bottom: -36px;" />
            </div>
        </div>
    </div>


    <div class="container p-5 ">
        <h1 class="text-left display-4 fw-bold mb-5">Qu'est-ce que le développement applicatif ?</h1>
        <div class="row justify-content-center">
            <div class="col-12">
                <p class="text-left">Le développement applicatif dans l’environnement web consiste à créer des programmes sur mesure qui fonctionnent via internet. Ces programmes, appelés applications web, offrent une variété de services aux utilisateurs, tels que la gestion de leur entreprise, la vente en ligne de produits ou services, ou encore l’automatisation de processus internes.</p>
            </div>
            <div class="col-12">
                <p class="text-left">Imaginez que vous souhaitez mettre en place un système de réservation en ligne pour votre petit hôtel ou un outil de gestion des stocks pour votre boutique en ligne. Le développement applicatif vous permettrait de créer ces solutions sur mesure, adaptées spécifiquement aux besoins de votre entreprise et accessibles depuis n’importe quel navigateur web, sur ordinateur, tablette ou smartphone.</p>
            </div>
            <div class="col-12">
                <p class="text-left">En somme, le développement applicatif web vous offre la possibilité de tirer parti des technologies numériques pour améliorer l’efficacité de votre entreprise, accroître sa visibilité en ligne et offrir une meilleure expérience à vos clients, le tout en restant flexible et évolutif pour s’adapter à vos besoins en constante évolution.</p>
            </div>
        </div>
    </div>



    <div class="container-fluid text-center mt-5 mb-5 p-5 ">
        <div class="accordion accordion-nofill" id="accordionFlushExample">
            <div class="accordion-item ms-md-5 ms-lg-0">
                <h2 class="accordion-header card-transparent">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne" style="font-weight: bold; font-size: 26px">
                        Quelles sont les étapes de développement d'une application ?
                    </button>
                </h2>
                <div id="flush-collapseOne" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body" style="font-size: 14px; text-align: left">Le processus de développement d’une application comprend plusieurs étapes cruciales.
                        Tout d’abord, il y a la phase de conception, où les besoins et les objectifs de l’application sont définis.
                        Ensuite vient la phase de planification, où l’équipe de développement élabore une feuille de route détaillée pour le projet,
                        en définissant les fonctionnalités, les délais et les ressources nécessaires. Ensuite, vient la phase de développement proprement dite,
                        où le code est écrit et les fonctionnalités sont implémentées. Cette phase est suivie de tests intensifs pour détecter et corriger les éventuels bugs et
                        assurer la qualité globale de l’application. Enfin, une fois que l’application est prête, elle est déployée et rendue disponible aux utilisateurs.
                        Tout au long de ce processus, la communication et la collaboration entre les membres de l’équipe de développement sont essentielles pour assurer le succès du projet.</div>
                </div>
            </div>
            <div class="accordion-item ms-md-5 ms-lg-0">
                <h2 class="accordion-header">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo" style="font-weight: bold;font-size: 26px">
                        Quels sont les types d'applications en informatique ?
                    </button>
                </h2>
                <div id="flush-collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body" style="font-size: 14px; text-align: left">Les types d’applications en informatique sont variés et peuvent inclure :
                        Applications de bureau : Ce sont des logiciels conçus pour être exécutés sur un ordinateur personnel.
                        Exemple : Microsoft Word, Adobe Photoshop. Applications web : Ce sont des programmes accessibles via un navigateur web.
                        Exemple : Gmail, Facebook. Applications mobiles : Ce sont des applications conçues spécifiquement pour fonctionner sur des appareils
                        mobiles tels que les smartphones et les tablettes. Exemple : Instagram, Uber. Applications embarquées : Ce sont des logiciels intégrés
                        dans des appareils électroniques tels que des GPS, des appareils électroménagers, etc. Exemple : Système de navigation d’une voiture, système
                        d’exploitation d’une Smart TV. Chaque type d’application a ses propres caractéristiques et avantages, et il peut être choisi en fonction des besoins spécifiques
                        d’utilisation et des plateformes cibles.</div>
                </div>
            </div>
            <div class="accordion-item ms-md-5 ms-lg-0"> <!-- Ajoutez les mêmes classes ici -->
                <h2 class="accordion-header">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree" style="font-weight: bold;font-size: 26px">
                        C'est quoi la différence entre application et logiciel ?
                    </button>
                </h2>
                <div id="flush-collapseThree" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body" style="font-size: 14px; text-align: left">En termes généraux, le terme « logiciel » est un concept plus large qui englobe tout programme informatique, y compris les applications.
                        Une application est un type spécifique de logiciel conçu pour effectuer une tâche ou un ensemble de tâches spécifiques pour l’utilisateur final. Pour clarifier la différence :
                        Logiciel : C’est un terme générique qui fait référence à tout programme informatique, qu’il s’agisse d’applications, de systèmes d’exploitation, de pilotes de périphériques, etc.
                        Les logiciels peuvent être classés en différentes catégories, y compris les applications. Application : C’est un type spécifique de logiciel qui est développé pour effectuer des tâches spécifiques
                        ou servir un objectif particulier pour l’utilisateur final. Les applications sont souvent interactives et utilisées pour des activités telles que la gestion de projet, la comptabilité, la gestion des ressources humaines, etc.
                        En résumé, toutes les applications sont des logiciels, mais tous les logiciels ne sont pas nécessairement des applications.</div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="container pt-2">
            <div class="container-fluid pt-5 my-5">
                <div class="text-center">
                    <p class="shorter-border-article mb-5"></p>
                    <h1 class="display-6 fw-bold pt-5">Nos derniers articles sur le développement informatique</h1>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="m-3 mt-md-5">
                @php
                    $articlesChunks = array_chunk($articles, 3);
                @endphp

                @foreach ($articlesChunks as $articlesRow)
                    <div class="row mb-4">
                        @foreach ($articlesRow as $article)
                            <div class="col-md-4">
                                <div class="d-flex flex-column h-100">
                                    <div class="bg-light shadow-lg rounded p-4 flex-grow-1 w-100 scale-on-hover">
                                        <h5 class="red fw-bolder mt-1 mb-4 text-md-start">{{ $article['attributes']['title'] }}</h5>
                                        @if(isset($article['attributes']['paragraphs']) && count($article['attributes']['paragraphs']) > 0)
                                            @php
                                                $firstParagraph = $article['attributes']['paragraphs'][0]['content'];
                                            @endphp
                                            <p>{!! substr($firstParagraph, 0, 112) !!}...</p>
                                        @endif
                                        <a href="{{ $article['attributes']['url'] }}" class="btn red text-decoration-underline">Lire la suite >></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>









@endsection
