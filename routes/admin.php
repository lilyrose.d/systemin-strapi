<?php


namespace App\Facades;

use Illuminate\Support\Facades\Facade;
use Laravel\Ui\UiServiceProvider;
use RuntimeException;

class Admin extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'admin';
    }

    public static function routes(array $options = [])
    {
        if (!static::$app->providerIsLoaded(UiServiceProvider::class)) {
            throw new RuntimeException('In order to use the Admin::routes() method, please install the laravel/ui package.');
        }

        static::$app->make('router')->admin($options);
    }
}
