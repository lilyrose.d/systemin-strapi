<?php


;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\Strapi\ArticleController;

use App\Http\Controllers\Strapi\ContentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
|be assigned to the "web" middleware group. Make something great!
|
*/





Route::get('/', [ArticleController::class, 'welcome'])->name('welcome');


Route::get('/maintenance-et-infogerance', function () {
    return view('seo.services.maintenance-et-infogerance');
})->name('seo.services.maintenance-et-infogerance');


Route::get('/a-propos', function () {
    return view('seo.a-propos');
})->name('seo.a-propos');


Route::get('/secteurs-dactivite', function () {
    return view('seo.secteur-activite');
})->name('seo.secteur-activite');



Route::get('/devis', function () {
    return view('seo.demande-devis');
})->name('seo.demande-devis');

Route::get('/application-de-gestion-de-fichiers-de-tarifs-pour-proprietes-viticoles', function () {
    return view('seo.secteurs-activites.bonhommie');
})->name('seo.secteurs-activites.bonhommie');

Route::get('/application-de-gestion-de-biens-immobiliers', function () {
    return view('seo.secteurs-activites.irragori');
})->name('seo.secteurs-activites.irragori');

Route::get('/application-web-de-gestion-de-remboursements-de-reparations', function () {
    return view('seo.secteurs-activites.irep');
})->name('seo.secteurs-activites.irep');

Route::get('/recrutement', function () {
    return view('seo.recrutement');
})->name('seo.recrutement');


Route::get('/conditions-generales-de-ventes', function () {
    return view('seo.cgv');
})->name('seo.cgv');

Route::get('/mentions-legales-et-politique-de-confidentialite', function () {
    return view('seo.mentions-legales');
})->name('seo.mentions-legales');




Route::post('/send-devis', [EmailController::class, 'send'])->name('seo.send-devis');

Route::get('/articles', [ArticleController::class, 'index'])->name('seo.articles.index');


Route::get('/{slug}', [ContentController::class, 'showBySlug'])->name('seo.content.show');


Route::post('/devis', [EmailController::class, 'send'])->name('seo.process-devis');










